//*******************************************************************
// Database
//
// Methods for creating, returning, and removing a connection to a database
//*******************************************************************

package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

    // Database connection parameters
    private static final String DBURL = "jdbc:mysql://wgudb.ucertify.com:3306/U05otS" +
            "?autoReconnect=true&useSSL=false";
    private static final String DBUSERNAME = "U05otS";
    private static final String DBPASSWORD = "53688564846";
    private static final String DBDRIVER = "com.mysql.jdbc.Driver";

    private static Connection conn = null;

    // Create a connection to the database
    public static Connection connect() {
        try {
            Class.forName(DBDRIVER);
            conn = DriverManager.getConnection(DBURL, DBUSERNAME, DBPASSWORD);
            System.out.println(Helper.changeColor("Database connection successful", Helper.Color.GREEN) + "\n");

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return conn;
    }

    // Close the connection to the database
    public static void disconnect() {
        try {
            conn.close();
            System.out.println(Helper.changeColor("Database connection closed", Helper.Color.GREEN));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Return database connection
    public static Connection getConnection() {
        return conn;
    }
}

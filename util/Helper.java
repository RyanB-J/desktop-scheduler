//*******************************************************************
// Helper
//
// Helper (convenience) methods for simplifying repetitive tasks
//*******************************************************************

package util;

public class Helper {

    // Convert strings to Title Case
    public static String toTitleCase(String input) {
        if(input == null || input.isEmpty())
            return "";

        if(input.length() == 1)
            return input.toUpperCase();

        // Split the string by space
        String[] parts = input.split(" ");

        StringBuilder sb = new StringBuilder(input.length());

        for(String part : parts) {

            if(part.length() > 1 )
                sb.append( part.substring(0, 1).toUpperCase() )
                        .append( part.substring(1).toLowerCase() );
            else sb.append(part.toUpperCase());

            sb.append(" ");
        }

        return sb.toString().trim();
    }

    // Available Text Colors
    public enum Color {RED, GREEN, YELLOW, BLUE}

    // Java console colors
    public static String changeColor(String text, Color color) {

        StringBuilder sb = new StringBuilder();

        // Append color code
        sb.append("\u001B[");
        switch (color) {
            case RED: sb.append("31m");
                break;
            case GREEN: sb.append("32m");
                break;
            case YELLOW: sb.append("33m");
                break;
            case BLUE: sb.append("34m");
                break;
            default:
                sb.append("0m");
                System.out.println("Warning: changeColor color parameter is invalid");
        }

        // Append text
        sb.append(text);

        // Append reset code
        sb.append("\u001B[0m");

        return sb.toString();
    }


}

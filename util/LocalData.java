//*******************************************************************
// LocalData
//
// Stores and controls all interactions to local data objects within the application.
//*******************************************************************

package util;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.DesktopScheduler;
import models.Appointment;
import models.Customer;
import models.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class LocalData {

    // Array lists of objects
    private final ObservableList<User> allUsers = FXCollections.observableArrayList();
    private final ObservableList<Customer> allCustomers = FXCollections.observableArrayList();
    private final ObservableList<Appointment> allAppointments = FXCollections.observableArrayList();

    // Create new objects
    public void addUser(User newUser) { allUsers.add(newUser); }
    public void addCustomer(Customer newCustomer) { allCustomers.add(newCustomer); }
    public void addAppointment(Appointment newAppointment) { allAppointments.add(newAppointment); }

    // Get all objects
    public ObservableList<User> getAllUsers() { return allUsers; }
    public ObservableList<Customer> getAllCustomers() { return allCustomers; }
    public ObservableList<Appointment> getAllAppointments() { return allAppointments; }

    // Get a list of User names
    public ObservableList<String> getAllUsersByName() {

        ObservableList<String> userNames = FXCollections.observableArrayList();

        // Lambda function is more concise and uses fewer lines of code
        getAllUsers().forEach(user -> userNames.add(user.getUserName()));

        return userNames;
    }

    // Get a list of Customer names
    public ObservableList<String> getAllCustomersByName() {

        ObservableList<String> customerNames = FXCollections.observableArrayList();

        // Lambda function is more concise and uses fewer lines of code
        getAllCustomers().forEach(customer -> customerNames.add(customer.getCustomerName()));

        return customerNames;
    }

    // Get a specific User by their userName
    public User getUserByName(String name) {
        User user = null;
        for (User item:allUsers) {
            if (item.getUserName().toLowerCase().equals(name.toLowerCase())) {
                user = item;
                break;
            }
        }
        return user;
    }

    // Get a specific Customer by their id
    public Customer getCustomerById(int id) {
        Customer customer = null;
        for (Customer item:allCustomers) {
            if (item.getId() == id) {
                customer = item;
                break;
            }
        }
        return customer;
    }

    // Get a list of Customers from a query
    public ObservableList<Customer> searchCustomers(String query, String property) {

        // Create an array of results that match the search field
        ObservableList<Customer> queryMatches = FXCollections.observableArrayList();
        for (Customer customer:getAllCustomers()) {

            // Search by customerName
            switch (property) {
                case "customerName":
                    if (customer.getCustomerName().toLowerCase().contains(query.toLowerCase())) {
                        queryMatches.add(customer);
                    }

                    // Search by address
                    break;
                case "address":
                    if (customer.getAddress1().toLowerCase().contains(query.toLowerCase())) {
                        queryMatches.add(customer);
                    }

                    // Search by phone
                    break;
                case "phone":
                    if (customer.getPhone().toLowerCase().contains(query.toLowerCase())) {
                        queryMatches.add(customer);
                    }
                    break;
            }
        }
        return queryMatches;
    }

    // Get a list of appointments by a date (restricted by user)
    public ObservableList<Appointment> getAppointmentsByDate(String date) {

        // Create an array of results that match the date
        ObservableList<Appointment> queryMatches = FXCollections.observableArrayList();
        for (Appointment appointment:getAllAppointments()) {
            if (appointment.getContact().equals(DesktopScheduler.getCurrentUser().getUserName())
                    && appointment.getStart().format(DateTimeFormatter.ofPattern("yyyy-MM-d")).equals(date)) {
                queryMatches.add(appointment);
            }
        }
        return queryMatches;
    }



    // Get a list of appointments by a year-month format (restricted by user)
    public ObservableList<Appointment> getAppointmentsByMonth(String date, String contact) {

        // Create an array of results that match the date
        ObservableList<Appointment> queryMatches = FXCollections.observableArrayList();
        for (Appointment appointment:getAllAppointments()) {
            if (appointment.getContact().equals(contact)
                    && appointment.getStart().format(DateTimeFormatter.ofPattern("yyyy-MM")).equals(date)) {
                queryMatches.add(appointment);
            }
        }
        return queryMatches;
    }

    // Get the number of appointments by type from a filtered list of appointments
    public int getAppointmentCountByType(ObservableList<Appointment> appointmentList, String type) {
        int count = 0;
        for (Appointment appointment:appointmentList) {
            if (appointment.getType().equals(type)) {
                count++;
            }
        }
        return count;
    }

    // Delete objects
    public void deleteCustomer(Customer customer) { allCustomers.remove(customer); }
    public void deleteAppointment(Appointment appointment) { allAppointments.remove(appointment); }

    // Create objects for all of the users, customers, and appointments in the database
    public void importAllData() {

        System.out.println(Helper.changeColor("Importing external data into memory...", Helper.Color.YELLOW));

        // Import all of the Users from the database
        String usersQuery = "SELECT `userId`, `userName`, `password`, `active` FROM `user`";
        try (Statement getUsers = Database.getConnection().createStatement()) {

            try (ResultSet results = getUsers.executeQuery(usersQuery)) {
                while (results.next()) {
                    User user = new User(
                            results.getInt("userId"),
                            results.getString("userName"),
                            results.getString("password"),
                            (results.getInt("active") == 1)
                    );
                    DesktopScheduler.getLocalData().addUser(user);
                }
            }

        } catch (SQLException e) {
            System.out.println("\t" + Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        if (DesktopScheduler.getLocalData().getAllUsers().isEmpty()) {
            System.out.println("\tNo users to create");
        } else {
            System.out.println("\t" + DesktopScheduler.getLocalData().getAllUsers().size() + " users created");
        }

        // Import all of the Customers from the database
        String customerQuery = "SELECT cu.`customerId`, cu.`customerName`, cu.`active`, cu.`addressId`, a.`address`, "
                + "a.`address2`, a.`postalCode`, a.`phone`, ci.`cityId`, ci.`city`, co.`country`"
                + " FROM `customer` cu"
                + " JOIN `address` a on cu.`addressId` = a.`addressId`"
                + " JOIN `city` ci on a.`cityId` = ci.`cityId`"
                + " JOIN `country` co on ci.`countryId` = co.`countryId`;";
        try (Statement getCustomers = Database.getConnection().createStatement()) {

            try (ResultSet resutls = getCustomers.executeQuery(customerQuery)) {
                while (resutls.next()) {
                    Customer customer = new Customer(
                            resutls.getInt("cu.customerId"),
                            resutls.getInt("cu.addressId"),
                            resutls.getInt("ci.cityId"),
                            resutls.getString("cu.customerName"),
                            resutls.getString("a.address"),
                            resutls.getString("a.address2"),
                            resutls.getString("a.postalCode"),
                            resutls.getString("a.phone"),
                            resutls.getString("ci.city"),
                            resutls.getString("co.country"),
                            (resutls.getInt("cu.active") == 1)
                    );
                    DesktopScheduler.getLocalData().addCustomer(customer);
                }
            }

        } catch (SQLException e) {
            System.out.println("\t" + Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        if (DesktopScheduler.getLocalData().getAllCustomers().isEmpty()) {
            System.out.println("\tNo customers to create");
        } else {
            System.out.println("\t" + DesktopScheduler.getLocalData().getAllCustomers().size() + " customers created");
        }

        // Import all of the appointments from the database
        String appointmentQuery = "SELECT `appointmentId`, `customerId`, `userId`, `title`, `description`, `location`, "
                + "`contact`, `type`, `url`, `start`, `end` FROM `appointment`";
        try (Statement getAppointments = Database.getConnection().createStatement()) {

            try (ResultSet results = getAppointments.executeQuery(appointmentQuery)) {
                while (results.next()) {

                    // Parse the timestamp results and translate them to the system date time
                    LocalDateTime startDateTime = LocalDateTime.parse(results.getString("start")
                            .substring(0, results.getString("start").indexOf(".")), // Remove .0 at the end
                            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                    LocalDateTime localStartDateTime = startDateTime.atZone(ZoneId.of("UTC"))
                            .withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();

                    LocalDateTime endDateTime = LocalDateTime.parse(results.getString("end")
                            .substring(0, results.getString("end").indexOf(".")), // Remove .0 at the end
                            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                    LocalDateTime localEndDateTime = endDateTime.atZone(ZoneId.of("UTC"))
                            .withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();

                    Appointment appointment = new Appointment(
                            results.getInt("appointmentId"),
                            results.getInt("customerId"),
                            results.getInt("userId"),
                            results.getString("title"),
                            results.getString("description"),
                            results.getString("location"),
                            results.getString("contact"),
                            results.getString("type"),
                            results.getString("url"),
                            localStartDateTime,
                            localEndDateTime
                    );
                    DesktopScheduler.getLocalData().addAppointment(appointment);
                }
            }

        } catch (SQLException e) {
            System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
        }

        if (DesktopScheduler.getLocalData().getAllAppointments().isEmpty()) {
            System.out.println("\tNo appointments to create");
        } else {
            System.out.println("\t" + DesktopScheduler.getLocalData().getAllAppointments().size()
                    + " appointments created");
        }

        System.out.println(Helper.changeColor("Import complete", Helper.Color.GREEN) + "\n");
    }

    // Customer Countries
    private final ObservableList<String> countries = FXCollections.observableArrayList("United States",
            "United Kingdom", "Germany");
    public ObservableList<String> getCountries() { return countries; }
    public int getCountryId(String country) {
        int id = 1;
        switch (country) {
            case "United States": id = 1;
                break;
            case "Germany": id = 2;
                break;
            case "United Kingdom": id = 3;
        }
        return id;
    }

    // Time Increments of all 48 possible times in increments of 30
    private final ObservableList<String> timeIncrements = FXCollections.observableArrayList("12:00 AM", "12:30 AM",
            "1:00 AM", "1:30 AM", "2:00 AM", "2:30 AM", "3:00 AM", "3:30 AM", "4:00 AM", "4:30 AM", "5:00 AM",
            "5:30 AM", "6:00 AM", "6:30 AM", "7:00 AM", "7:30 AM", "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM",
            "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM",
            "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM", "6:00 PM", "6:30 PM",
            "7:00 PM", "7:30 PM", "8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM", "10:00 PM", "10:30 PM", "11:00 PM",
            "11:30 PM");
    public ObservableList<String> getTimeIncrements() { return timeIncrements; }

    // Business Hours
    public final String openingTime = "6:00 AM";
    public final String closingTime = "6:00 PM";

    // Appointment Types
    private final ObservableList<String> appointmentTypes = FXCollections.observableArrayList(
            "Business", "Personal", "Training");
    public ObservableList<String> getAppointmentTypes() {
        return appointmentTypes;
    }

    // Report Types
    private final ObservableList<String> reportTypes = FXCollections.observableArrayList(
            "Number of appointment types by month", "Schedule for each consultant",
            "Hours worked for each consultant");
    public ObservableList<String> getReportTypes() {
        return reportTypes;
    }
}

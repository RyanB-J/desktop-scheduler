//*******************************************************************
// Logger
//
// Logs messages to a text file, including the time of occurrence and type of log.
//*******************************************************************

package util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {

    // Date Formats
    private static final DateTimeFormatter logDateTimeFormatter = DateTimeFormatter.ofPattern("MMMM d, yyyy 'at' h:mm a");

    // Global Variables
    public enum Type {ERROR, INFO}

    // Add text to a file
    public static void log(Type type, String text) {

        try (FileWriter fWriter = new FileWriter("src/log.txt", true);
                BufferedWriter bWriter = new BufferedWriter(fWriter);
                PrintWriter pWriter = new PrintWriter(bWriter)) {
            pWriter.println(LocalDateTime.now().format(logDateTimeFormatter)
                    + "[" + type + "]" + ": " + text);

        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }
}

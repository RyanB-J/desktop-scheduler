//*******************************************************************
// Validation
//
// Methods for formatting and/or validating form fields.
//*******************************************************************

package util;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

public class Validation {

    // Text Field Length Limiter
    public static void formatTextField(final TextField textField, final int maxLength, final boolean isNumber) {
        textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {

                if (textField.getText().length() < maxLength) {
                    if (isNumber && !textField.getText().matches("^\\d*$")) {
                        textField.setText(newValue.replaceAll("\\D", ""));
                    }
                } else {
                    textField.setText(textField.getText().substring(0, maxLength));
                }
            }
        });
    }
}
///////////////////////////////////////////////////////////////////////////////
//
// Title:   Desktop Scheduler
// Course:  C195 - Software 2
// Author:  Ryan Bains-Jordan
// Email:   rbains1@wgu.edu
//
///////////////////////////////////////////////////////////////////////////////

package main;

import controllers.LogInController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import models.User;
import util.Database;
import util.Helper;
import util.LocalData;

import java.io.IOException;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.Locale;

public class DesktopScheduler extends Application {

    // State Variables
    public static Stage stage;
    private static final LocalData localData = new LocalData();
    private static String startScreen = "LogIn";

    // Current User
    private static User currentUser = null;
    public static User getCurrentUser() {
        return currentUser;
    }
    public static void setCurrentUser(User currentUser) {
        DesktopScheduler.currentUser = currentUser;
    }

    // Test method to start on a different screen
    private static void developerMode(String user, String startingScreen) {
        setCurrentUser(DesktopScheduler.getLocalData().getUserByName(user));
        startScreen = startingScreen;
        System.out.println(Helper.changeColor("Developer Mode Activated", Helper.Color.GREEN));
        System.out.println("\tLogged in as " + Helper.changeColor(currentUser.getUserName(), Helper.Color.BLUE));
        System.out.println("\tStarting on screen: " + Helper.changeColor(startScreen, Helper.Color.BLUE) + "\n");
    }

    // General scene changer for all scenes
    public static void changeScenes(String FXMLFile) throws IOException {
        Parent viewFile = FXMLLoader.load(LogInController.class.getResource("../views/" + FXMLFile + ".fxml"));
        Scene scene = new Scene(viewFile);

        Stage stage = getStage();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../views/" + startScreen + ".fxml"));

        Scene scene = new Scene(root);
        stage = primaryStage;
        stage.setTitle("Desktop Scheduler");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public static Stage getStage() { return stage; }

    public static LocalData getLocalData() { return localData; }

    public static void main(String[] args) throws SQLException {
        Database.connect();

        // Determine Locale and TimeZone
        System.out.println("Using time zone: " + ZoneId.systemDefault());
        System.out.println("Using language: " + Locale.getDefault() + "\n");

        // Import all database records into local data objects
        localData.importAllData();

        // Developer Mode - (Uncomment to use) Automatically Logs In as a user and navigates to the specified screen
        //developerMode("test", "AppointmentsMonthly");

        launch(args);
        Database.disconnect();
    }
}

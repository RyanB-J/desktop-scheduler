//*******************************************************************
// AppointmentsMonthlyController
//
// Controls the interactions for AppointmentsMonthly.fxml. This shows appointments on a monthly interactive calendar
// (based on who is logged in) and allows the user to modify them.
//*******************************************************************

package controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import main.DesktopScheduler;
import models.Appointment;
import util.Database;
import util.Helper;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class AppointmentsMonthlyController implements Initializable {

    // Components
    @FXML private Label calendarTitleLabel;
    @FXML private Pane calendarCell_1, calendarCell_2, calendarCell_3, calendarCell_4, calendarCell_5, calendarCell_6,
            calendarCell_7, calendarCell_8, calendarCell_9, calendarCell_10, calendarCell_11, calendarCell_12,
            calendarCell_13, calendarCell_14, calendarCell_15, calendarCell_16, calendarCell_17, calendarCell_18,
            calendarCell_19, calendarCell_20, calendarCell_21, calendarCell_22, calendarCell_23, calendarCell_24,
            calendarCell_25, calendarCell_26, calendarCell_27, calendarCell_28, calendarCell_29, calendarCell_30,
            calendarCell_31, calendarCell_32, calendarCell_33, calendarCell_34, calendarCell_35, calendarCell_36,
            calendarCell_37, calendarCell_38, calendarCell_39, calendarCell_40, calendarCell_41, calendarCell_42;
    @FXML private Text calendarCellNum_1, calendarCellNum_2, calendarCellNum_3, calendarCellNum_4, calendarCellNum_5,
            calendarCellNum_6, calendarCellNum_7, calendarCellNum_8, calendarCellNum_9, calendarCellNum_10,
            calendarCellNum_11, calendarCellNum_12, calendarCellNum_13, calendarCellNum_14, calendarCellNum_15,
            calendarCellNum_16, calendarCellNum_17, calendarCellNum_18, calendarCellNum_19, calendarCellNum_20,
            calendarCellNum_21, calendarCellNum_22, calendarCellNum_23, calendarCellNum_24, calendarCellNum_25,
            calendarCellNum_26, calendarCellNum_27, calendarCellNum_28, calendarCellNum_29, calendarCellNum_30,
            calendarCellNum_31, calendarCellNum_32, calendarCellNum_33, calendarCellNum_34, calendarCellNum_35,
            calendarCellNum_36, calendarCellNum_37, calendarCellNum_38, calendarCellNum_39, calendarCellNum_40,
            calendarCellNum_41, calendarCellNum_42;
    @FXML private Button deleteButton;
    @FXML private Button editButton;

    // Appointment Table
    @FXML private TableView<Appointment> appointmentTableView;
    @FXML private TableColumn<Appointment, String>appointmentTableColumn;

    // Appointment Table Items
    private final ObservableList<Appointment> appointmentTableItems = FXCollections.observableArrayList();

    // LinkedHashMap for creating an iterable relationship between two JavaFX components
    private final LinkedHashMap<Pane, Text> calendarHashMap = new LinkedHashMap<>();

    // Java Calendar Instance
    private final Calendar calendar = Calendar.getInstance();

    // Date Formats
    private final SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM");
    private final SimpleDateFormat monthYearFormat = new SimpleDateFormat("MMMM yyyy");
    private final SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final SimpleDateFormat isoSingleDateFormat = new SimpleDateFormat("yyyy-MM-d");
    private final SimpleDateFormat isoYearMonthFormat = new SimpleDateFormat("yyyy-MM");
    private final DateTimeFormatter isoDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    // State Variables
    private Pane currentCalendarCell = null;
    private static Appointment selectedAppointment = null;

    // Local Variables
    private Set<Pane> calendarHashMapSet = null;

    // Getters
    public static Appointment getSelectedAppointment() {
        return selectedAppointment;
    }

    // Setters
    public static void setSelectedAppointment(Appointment appointment) {
        selectedAppointment = appointment;
    }

    // Delete Customer Alert
    private static final Alert deleteAlert = new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure? This action can not be undone.",
            ButtonType.YES, ButtonType.NO);

    // Clear State Variables
    private void deselectCell() {
        if (currentCalendarCell != null) {
            currentCalendarCell.getStyleClass().remove("cal-selected-cell");
        }
        currentCalendarCell = null;
        appointmentTableColumn.setText("No day selected");
        appointmentTableItems.clear();
        appointmentTableView.setItems(appointmentTableItems);
    }

    // Actions to set a Clicked Calendar Cell as Selected
    private void selectCell(Pane calendarCell, Text calendarCellNum) {

        // Remove the border from the old cell
        if (currentCalendarCell != null) {
            currentCalendarCell.getStyleClass().remove("cal-selected-cell");
        }

        // Change the border of the new cell
        currentCalendarCell = calendarCell;
        currentCalendarCell.getStyleClass().add("cal-selected-cell");

        // Change the header text of the appointments table
        appointmentTableColumn.setText(monthFormat.format(calendar.getTime()) + " " + calendarCellNum.getText());

        // Clear the table of existing appointments
        appointmentTableItems.clear();
        appointmentTableView.setItems(appointmentTableItems);
        
        // Get the appointments for the day (if any)
        String selectedDay = isoYearMonthFormat.format(calendar.getTime()) + "-" + calendarCellNum.getText();
        ObservableList<Appointment> currentAppointments =
                DesktopScheduler.getLocalData().getAppointmentsByDate(selectedDay);

        if (!currentAppointments.isEmpty()) {
            appointmentTableItems.addAll(currentAppointments);
            appointmentTableView.setItems(appointmentTableItems);
        }

        // Set Edit and Delete buttons to disabled
        editButton.setDisable(true);
        deleteButton.setDisable(true);
    }

    // Generate the Calendar for the Month
    private void generateCalendar() {

        // Set calendar initial values
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
        int calendarMapIndex = 1;
        boolean endOfMonth = false;

        // Iterate through the cells and populate them with correct day number
        for (Pane pane:calendarHashMapSet) {

            // If the calendar index is greater-than or equal to the first weekday of the month, start the iteration
            if (calendarMapIndex >= calendar.get(Calendar.DAY_OF_WEEK) && !endOfMonth) {
                calendarHashMap.get(pane).setText(Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
                pane.getStyleClass().remove("cal-empty-cell");

                // Check if any appointments are on this day
                if (!DesktopScheduler.getLocalData().getAppointmentsByDate(isoSingleDateFormat.format(calendar.getTime())).isEmpty()) {
                    if (!calendarHashMap.get(pane).getStyleClass().contains("cal-bold-text")) {
                        calendarHashMap.get(pane).getStyleClass().add("cal-bold-text");
                    }
                } else {
                    calendarHashMap.get(pane).getStyleClass().remove("cal-bold-text");
                }

                // Check if this day is today's date
                if (isoDateFormat.format(calendar.getTime()).equals(LocalDateTime.now().format(isoDateFormatter))) {
                    if (!calendarHashMap.get(pane).getStyleClass().contains("cal-blue-text")) {
                        calendarHashMap.get(pane).getStyleClass().add("cal-blue-text");
                        selectCell(pane, calendarHashMap.get(pane));
                    }
                } else {
                    calendarHashMap.get(pane).getStyleClass().remove("cal-blue-text");
                }

                if (calendar.get(Calendar.DAY_OF_MONTH) != calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                    calendar.add(Calendar.DAY_OF_MONTH, 1);
                } else {
                    endOfMonth = true;
                }

            // Otherwise remove the text and color the pane gray
            } else {
                calendarHashMap.get(pane).setText("");
                if (!pane.getStyleClass().contains("cal-empty-cell")) {
                    pane.getStyleClass().add("cal-empty-cell");
                }
            }

            calendarMapIndex++;
        }
    }

    // Navigation Controls
    @FXML private void navbarCustomersButton(ActionEvent event) throws IOException {
        deselectCell();
        DesktopScheduler.changeScenes("Customers");
    }
    @FXML private void navbarReportsButton(ActionEvent event) throws IOException {
        deselectCell();
        DesktopScheduler.changeScenes("ReportAppointmentType");
    }
    @FXML private void navbarLogOutButton(ActionEvent event) throws IOException {
        deselectCell();
        System.out.println("Logged out\n");
        DesktopScheduler.changeScenes("LogIn");
    }
    
    // Toggle Buttons
    @FXML private void weeklyToggleButton(ActionEvent event) {
        deselectCell();
        try {
            DesktopScheduler.changeScenes("AppointmentsWeekly");
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }

    // Calendar Controls
    @FXML private void calendarPreviousButton(ActionEvent event) {
        calendar.add(Calendar.MONTH, -1);
        calendarTitleLabel.setText(monthYearFormat.format(calendar.getTime()));
        generateCalendar();
        deselectCell();
    }
    @FXML private void calendarNextButton(ActionEvent event) {
        calendar.add(Calendar.MONTH, 1);
        calendarTitleLabel.setText(monthYearFormat.format(calendar.getTime()));
        generateCalendar();
        deselectCell();
    }

    // Calendar Cells
    @FXML private void calendarCellClicked_1(MouseEvent event) {selectCell(calendarCell_1, calendarCellNum_1);}
    @FXML private void calendarCellClicked_2(MouseEvent event) {selectCell(calendarCell_2, calendarCellNum_2);}
    @FXML private void calendarCellClicked_3(MouseEvent event) {selectCell(calendarCell_3, calendarCellNum_3);}
    @FXML private void calendarCellClicked_4(MouseEvent event) {selectCell(calendarCell_4, calendarCellNum_4);}
    @FXML private void calendarCellClicked_5(MouseEvent event) {selectCell(calendarCell_5, calendarCellNum_5);}
    @FXML private void calendarCellClicked_6(MouseEvent event) {selectCell(calendarCell_6, calendarCellNum_6);}
    @FXML private void calendarCellClicked_7(MouseEvent event) {selectCell(calendarCell_7, calendarCellNum_7);}
    @FXML private void calendarCellClicked_8(MouseEvent event) {selectCell(calendarCell_8, calendarCellNum_8);}
    @FXML private void calendarCellClicked_9(MouseEvent event) {selectCell(calendarCell_9, calendarCellNum_9);}
    @FXML private void calendarCellClicked_10(MouseEvent event) {selectCell(calendarCell_10, calendarCellNum_10);}
    @FXML private void calendarCellClicked_11(MouseEvent event) {selectCell(calendarCell_11, calendarCellNum_11);}
    @FXML private void calendarCellClicked_12(MouseEvent event) {selectCell(calendarCell_12, calendarCellNum_12);}
    @FXML private void calendarCellClicked_13(MouseEvent event) {selectCell(calendarCell_13, calendarCellNum_13);}
    @FXML private void calendarCellClicked_14(MouseEvent event) {selectCell(calendarCell_14, calendarCellNum_14);}
    @FXML private void calendarCellClicked_15(MouseEvent event) {selectCell(calendarCell_15, calendarCellNum_15);}
    @FXML private void calendarCellClicked_16(MouseEvent event) {selectCell(calendarCell_16, calendarCellNum_16);}
    @FXML private void calendarCellClicked_17(MouseEvent event) {selectCell(calendarCell_17, calendarCellNum_17);}
    @FXML private void calendarCellClicked_18(MouseEvent event) {selectCell(calendarCell_18, calendarCellNum_18);}
    @FXML private void calendarCellClicked_19(MouseEvent event) {selectCell(calendarCell_19, calendarCellNum_19);}
    @FXML private void calendarCellClicked_20(MouseEvent event) {selectCell(calendarCell_20, calendarCellNum_20);}
    @FXML private void calendarCellClicked_21(MouseEvent event) {selectCell(calendarCell_21, calendarCellNum_21);}
    @FXML private void calendarCellClicked_22(MouseEvent event) {selectCell(calendarCell_22, calendarCellNum_22);}
    @FXML private void calendarCellClicked_23(MouseEvent event) {selectCell(calendarCell_23, calendarCellNum_23);}
    @FXML private void calendarCellClicked_24(MouseEvent event) {selectCell(calendarCell_24, calendarCellNum_24);}
    @FXML private void calendarCellClicked_25(MouseEvent event) {selectCell(calendarCell_25, calendarCellNum_25);}
    @FXML private void calendarCellClicked_26(MouseEvent event) {selectCell(calendarCell_26, calendarCellNum_26);}
    @FXML private void calendarCellClicked_27(MouseEvent event) {selectCell(calendarCell_27, calendarCellNum_27);}
    @FXML private void calendarCellClicked_28(MouseEvent event) {selectCell(calendarCell_28, calendarCellNum_28);}
    @FXML private void calendarCellClicked_29(MouseEvent event) {selectCell(calendarCell_29, calendarCellNum_29);}
    @FXML private void calendarCellClicked_30(MouseEvent event) {selectCell(calendarCell_30, calendarCellNum_30);}
    @FXML private void calendarCellClicked_31(MouseEvent event) {selectCell(calendarCell_31, calendarCellNum_31);}
    @FXML private void calendarCellClicked_32(MouseEvent event) {selectCell(calendarCell_32, calendarCellNum_32);}
    @FXML private void calendarCellClicked_33(MouseEvent event) {selectCell(calendarCell_33, calendarCellNum_33);}
    @FXML private void calendarCellClicked_34(MouseEvent event) {selectCell(calendarCell_34, calendarCellNum_34);}
    @FXML private void calendarCellClicked_35(MouseEvent event) {selectCell(calendarCell_35, calendarCellNum_35);}
    @FXML private void calendarCellClicked_36(MouseEvent event) {selectCell(calendarCell_36, calendarCellNum_36);}
    @FXML private void calendarCellClicked_37(MouseEvent event) {selectCell(calendarCell_37, calendarCellNum_37);}
    @FXML private void calendarCellClicked_38(MouseEvent event) {selectCell(calendarCell_38, calendarCellNum_38);}
    @FXML private void calendarCellClicked_39(MouseEvent event) {selectCell(calendarCell_39, calendarCellNum_39);}
    @FXML private void calendarCellClicked_40(MouseEvent event) {selectCell(calendarCell_40, calendarCellNum_40);}
    @FXML private void calendarCellClicked_41(MouseEvent event) {selectCell(calendarCell_41, calendarCellNum_41);}
    @FXML private void calendarCellClicked_42(MouseEvent event) {selectCell(calendarCell_42, calendarCellNum_42);}

    // Appointment Controls
    @FXML private void addButton(ActionEvent event) {
        try {
            AddAppointmentController.setLastScene("AppointmentsMonthly");
            DesktopScheduler.changeScenes("AddAppointment");
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }
    @FXML private void editButton(ActionEvent event) {
        try {
            EditAppointmentController.setLastScene("AppointmentsMonthly");
            DesktopScheduler.changeScenes("EditAppointment");
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }
    @FXML private void deleteButton(ActionEvent event) {
        deleteAlert.showAndWait();
        if (deleteAlert.getResult() == ButtonType.YES) {

            System.out.println(Helper.changeColor("Deleting Appointment...", Helper.Color.YELLOW));

            String appointmentDelete = "DELETE FROM `appointment` WHERE `appointmentId` = ?";
            try (PreparedStatement deleteAppointment = Database.getConnection().prepareStatement(appointmentDelete)) {
                deleteAppointment.setInt(1, AppointmentsMonthlyController.getSelectedAppointment().getId());

                // Delete the record
                int rowsAffected = deleteAppointment.executeUpdate();
                System.out.println("\tRemoved Appointment: " + rowsAffected + " row affected.");
                if (rowsAffected == 0) System.out.println("\tAppointment Deletion Failed");

                // Update appointment array
                DesktopScheduler.getLocalData().deleteAppointment(getSelectedAppointment());

                // Update the appointment tables
                appointmentTableView.getSelectionModel().select(null);
                deselectCell();
                generateCalendar();
                editButton.setDisable(true);
                deleteButton.setDisable(true);

                System.out.println(Helper.changeColor("Appointment deleted", Helper.Color.GREEN));
                System.out.println(DesktopScheduler.getLocalData().getAllAppointments().size() + " appointments in memory\n");


            } catch (SQLException e) {
                System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Set Edit and Delete buttons to disabled
        editButton.setDisable(true);
        deleteButton.setDisable(true);

        // Configure Calendar Title
        calendarTitleLabel.setText(monthYearFormat.format(calendar.getTime()));

        // Configure Appointment Table
        appointmentTableColumn.setCellValueFactory(new PropertyValueFactory<Appointment, String>("title"));
        appointmentTableColumn.setText("No day selected");
        appointmentTableView.setItems(appointmentTableItems);

        // Configure Calendar Map
        calendarHashMap.put(calendarCell_1, calendarCellNum_1);
        calendarHashMap.put(calendarCell_2, calendarCellNum_2);
        calendarHashMap.put(calendarCell_3, calendarCellNum_3);
        calendarHashMap.put(calendarCell_4, calendarCellNum_4);
        calendarHashMap.put(calendarCell_5, calendarCellNum_5);
        calendarHashMap.put(calendarCell_6, calendarCellNum_6);
        calendarHashMap.put(calendarCell_7, calendarCellNum_7);
        calendarHashMap.put(calendarCell_8, calendarCellNum_8);
        calendarHashMap.put(calendarCell_9, calendarCellNum_9);
        calendarHashMap.put(calendarCell_10, calendarCellNum_10);
        calendarHashMap.put(calendarCell_11, calendarCellNum_11);
        calendarHashMap.put(calendarCell_12, calendarCellNum_12);
        calendarHashMap.put(calendarCell_13, calendarCellNum_13);
        calendarHashMap.put(calendarCell_14, calendarCellNum_14);
        calendarHashMap.put(calendarCell_15, calendarCellNum_15);
        calendarHashMap.put(calendarCell_16, calendarCellNum_16);
        calendarHashMap.put(calendarCell_17, calendarCellNum_17);
        calendarHashMap.put(calendarCell_18, calendarCellNum_18);
        calendarHashMap.put(calendarCell_19, calendarCellNum_19);
        calendarHashMap.put(calendarCell_20, calendarCellNum_20);
        calendarHashMap.put(calendarCell_21, calendarCellNum_21);
        calendarHashMap.put(calendarCell_22, calendarCellNum_22);
        calendarHashMap.put(calendarCell_23, calendarCellNum_23);
        calendarHashMap.put(calendarCell_24, calendarCellNum_24);
        calendarHashMap.put(calendarCell_25, calendarCellNum_25);
        calendarHashMap.put(calendarCell_26, calendarCellNum_26);
        calendarHashMap.put(calendarCell_27, calendarCellNum_27);
        calendarHashMap.put(calendarCell_28, calendarCellNum_28);
        calendarHashMap.put(calendarCell_29, calendarCellNum_29);
        calendarHashMap.put(calendarCell_30, calendarCellNum_30);
        calendarHashMap.put(calendarCell_31, calendarCellNum_31);
        calendarHashMap.put(calendarCell_32, calendarCellNum_32);
        calendarHashMap.put(calendarCell_33, calendarCellNum_33);
        calendarHashMap.put(calendarCell_34, calendarCellNum_34);
        calendarHashMap.put(calendarCell_35, calendarCellNum_35);
        calendarHashMap.put(calendarCell_36, calendarCellNum_36);
        calendarHashMap.put(calendarCell_37, calendarCellNum_37);
        calendarHashMap.put(calendarCell_38, calendarCellNum_38);
        calendarHashMap.put(calendarCell_39, calendarCellNum_39);
        calendarHashMap.put(calendarCell_40, calendarCellNum_40);
        calendarHashMap.put(calendarCell_41, calendarCellNum_41);
        calendarHashMap.put(calendarCell_42, calendarCellNum_42);
        calendarHashMapSet = calendarHashMap.keySet();

        // Generate the calendar
        generateCalendar();

        // Appointment Table Selection Listener
        appointmentTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Appointment>() {
            @Override
            public void changed(ObservableValue<? extends Appointment> observable, Appointment oldValue, Appointment newValue) {
                if (appointmentTableView.getSelectionModel().getSelectedItem() != null) {
                    selectedAppointment = newValue;
                    editButton.setDisable(false);
                    deleteButton.setDisable(false);
                }
            }
        });
    }
}

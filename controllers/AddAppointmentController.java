//*******************************************************************
// AddAppointmentController
//
// Controls the interactions for AddAppointment.fxml. This is used to add appointments to the application.
//*******************************************************************

package controllers;

import exceptions.InvalidDataException;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.DesktopScheduler;
import models.Appointment;
import models.Customer;
import exceptions.AppointmentConflictException;
import util.Database;
import util.Helper;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class AddAppointmentController implements Initializable {

    // Components
    @FXML private TextField titleTextField;
    @FXML private ChoiceBox<String> typeChoiceBox;
    @FXML private TextArea descriptionTextArea;
    @FXML private TextField urlTextField;
    @FXML private ChoiceBox<String> customerChoiceBox;
    @FXML private TextField locationTextField;
    @FXML private DatePicker dateDatePicker;
    @FXML private ComboBox<String> startTimeComboBox;
    @FXML private ComboBox<String> endTimeComboBox;
    @FXML private Label businessHoursLabel;

    // State variables
    private Customer selectedCustomer;
    private static String lastScene = "AppointmentsMonthly";

    // Setters
    public static void setLastScene(String currentScene) { lastScene = currentScene; }

    // Date Formats
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd h:mm a");

    // Navigation Controls
    @FXML private void navbarCustomersButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("Customers");
    }
    @FXML private void navbarReportsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("ReportAppointmentType");
    }
    @FXML private void navbarLogOutButton(ActionEvent event) throws IOException {
        System.out.println("Logged out\n");
        DesktopScheduler.changeScenes("LogIn");
    }

    // Alerts
    private final Alert customerActiveAlert = new Alert(Alert.AlertType.ERROR,
            "Customer account not active.", ButtonType.CLOSE);
    private final Alert timeDurationAlert = new Alert(Alert.AlertType.ERROR,
            "Time duration is invalid.", ButtonType.CLOSE);
    private final Alert businessHoursAlert = new Alert(Alert.AlertType.ERROR,
            "Appointment is outside of business hours.", ButtonType.CLOSE);

    private void goToPreviousScene() {
        if (lastScene.equals("AppointmentsMonthly")) {
            try {
                DesktopScheduler.changeScenes("AppointmentsMonthly");
            } catch (IOException e) {
                System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
            }
        } else if (lastScene.equals("AppointmentsWeekly")) {
            try {
                DesktopScheduler.changeScenes("AppointmentsWeekly");
            } catch (IOException e) {
                System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
            }
        }
    }

    // Validate Form
    private boolean formValidated() {

        // Is customer active
        if (!selectedCustomer.isActive()) {
            customerActiveAlert.showAndWait();
            return false;
        }

        // Is endTime before startTime
        else if (endTimeComboBox.getSelectionModel().getSelectedIndex()
                <= startTimeComboBox.getSelectionModel().getSelectedIndex()) {
            timeDurationAlert.showAndWait();
            return false;
        }

        // Is the appointment within business hours
        ObservableList<String> timeIncrements = DesktopScheduler.getLocalData().getTimeIncrements();
        int openTimeIndex = timeIncrements.indexOf(DesktopScheduler.getLocalData().openingTime);
        int closeTimeIndex = timeIncrements.indexOf(DesktopScheduler.getLocalData().closingTime);
        int startTimeIndex = timeIncrements.indexOf(startTimeComboBox.getValue());
        int endTimeIndex = timeIncrements.indexOf(endTimeComboBox.getValue());
        if (startTimeIndex < openTimeIndex || endTimeIndex > closeTimeIndex) {
            businessHoursAlert.showAndWait();
            throw new InvalidDataException("Start Time and/or End Time are outside of Business Hours.");
        }

        // Is the appointment clashing with another
        LocalDateTime startLocalDateTime = LocalDateTime.parse(dateDatePicker.getValue() + " "
                + startTimeComboBox.getValue(), dateTimeFormatter);
        LocalDateTime endLocalDateTime = LocalDateTime.parse(dateDatePicker.getValue() + " "
                + endTimeComboBox.getValue(), dateTimeFormatter);

        for (Appointment appointment:DesktopScheduler.getLocalData().getAppointmentsByDate(dateDatePicker.getValue().toString())) {
            if (!(startLocalDateTime.isAfter(appointment.getEnd()) || endLocalDateTime.isBefore(appointment.getStart()))) {
                throw new AppointmentConflictException("Appointment is conflicting with an existing appointment.");
            }
        }
        return true;
    }

    @FXML private void addButton(ActionEvent event) {

        // Get the selected customer
        selectedCustomer = DesktopScheduler.getLocalData().getAllCustomers()
                .get(customerChoiceBox.getSelectionModel().getSelectedIndex());

        if (formValidated()) {

            System.out.println(Helper.changeColor("Creating an Appointment...", Helper.Color.YELLOW));

            // Create an Appointment object
            Appointment newAppointment = new Appointment();

            // Create LocalDateTimes for the new appointment object
            LocalDateTime startLocalDateTime = LocalDateTime.parse(dateDatePicker.getValue() + " "
                    + startTimeComboBox.getValue(), dateTimeFormatter);
            LocalDateTime endLocalDateTime = LocalDateTime.parse(dateDatePicker.getValue() + " "
                    + endTimeComboBox.getValue(), dateTimeFormatter);

            // Convert the dateTime to UTC
            LocalDateTime utcStartDateTime = (startLocalDateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime());
            LocalDateTime utcEndDateTime = (endLocalDateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime());
            LocalDateTime utcCurrentDateTime = (LocalDateTime.now().atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime());

            // Add the appointment to the database
            String appointmentInsert = "INSERT INTO `appointment`"
                    + " (`customerId`, `userId`, `title`, `description`, `location`, `contact`, `type`, `url`, `start`, "
                    + "`end`, `createDate`, `createdBy`, `lastUpdateBy`)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            try (PreparedStatement createAppointment = Database.getConnection().prepareStatement(appointmentInsert,
                    Statement.RETURN_GENERATED_KEYS)) {
                createAppointment.setInt(1, selectedCustomer.getId());
                createAppointment.setInt(2, DesktopScheduler.getCurrentUser().getId());
                createAppointment.setString(3, Helper.toTitleCase(titleTextField.getText()));
                createAppointment.setString(4, descriptionTextArea.getText());
                createAppointment.setString(5, Helper.toTitleCase(locationTextField.getText()));
                createAppointment.setString(6, DesktopScheduler.getCurrentUser().getUserName());
                createAppointment.setString(7, typeChoiceBox.getValue());
                createAppointment.setString(8, urlTextField.getText());
                createAppointment.setTimestamp(9, Timestamp.valueOf(utcStartDateTime));
                createAppointment.setTimestamp(10, Timestamp.valueOf(utcEndDateTime));
                createAppointment.setObject(11, Timestamp.valueOf(utcCurrentDateTime));
                createAppointment.setString(12, DesktopScheduler.getCurrentUser().getUserName());
                createAppointment.setString(13, DesktopScheduler.getCurrentUser().getUserName());

                // Insert the record
                int rowsAffected = createAppointment.executeUpdate();
                System.out.println("\tCreated Appointment: " + rowsAffected + " row affected.");
                if (rowsAffected == 0) System.out.println("\tAppointment Creation Failed");

                // Get the key of the insert
                try (ResultSet generatedKey = createAppointment.getGeneratedKeys()) {
                    if (generatedKey.next()) {
                        newAppointment.setId(generatedKey.getInt(1));
                    }
                }

                // Add to the new appointment object
                newAppointment.setCustomerId(selectedCustomer.getId());
                newAppointment.setUserId(DesktopScheduler.getCurrentUser().getId());
                newAppointment.setTitle(Helper.toTitleCase(titleTextField.getText()));
                newAppointment.setDescription(descriptionTextArea.getText());
                newAppointment.setLocation(Helper.toTitleCase(locationTextField.getText()));
                newAppointment.setContact(DesktopScheduler.getCurrentUser().getUserName());
                newAppointment.setType(typeChoiceBox.getValue());
                newAppointment.setUrl(urlTextField.getText());
                newAppointment.setStart(startLocalDateTime);
                newAppointment.setEnd(endLocalDateTime);

                System.out.println(Helper.changeColor("Appointment Created", Helper.Color.GREEN));

                DesktopScheduler.getLocalData().addAppointment(newAppointment);
                System.out.println(DesktopScheduler.getLocalData().getAllAppointments().size()
                        + " appointments in memory\n");

            } catch (SQLException e) {
                System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
            }

            // Go back to Appointments screen
            goToPreviousScene();
        }
    }

    // Cancel the form
    @FXML private void cancelButton(ActionEvent event) {
        goToPreviousScene();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Configure Type Choice Box
        typeChoiceBox.setItems(DesktopScheduler.getLocalData().getAppointmentTypes());
        typeChoiceBox.setValue("Business");

        // Configure Customer Choice Box
        customerChoiceBox.setItems(DesktopScheduler.getLocalData().getAllCustomersByName());
        customerChoiceBox.setValue(customerChoiceBox.getItems().get(0));

        // Configure Date Picker
        dateDatePicker.setValue(LocalDateTime.now().toLocalDate());

        // Configure Time Choice Box
        startTimeComboBox.setItems(DesktopScheduler.getLocalData().getTimeIncrements());
        startTimeComboBox.setValue("9:00 AM");
        endTimeComboBox.setItems(DesktopScheduler.getLocalData().getTimeIncrements());
        endTimeComboBox.setValue("10:00 AM");

        // Configure Business Hours Label
        businessHoursLabel.setText(DesktopScheduler.getLocalData().openingTime
                + " - " + DesktopScheduler.getLocalData().closingTime);
    }
}

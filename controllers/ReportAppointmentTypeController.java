//*******************************************************************
// ReportAppointmentTypeController
//
// Controls the interactions for ReportAppointmentType.fxml. This shows the amount of each type of appointment created
// per month.
//*******************************************************************

package controllers;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;
import javafx.util.Callback;
import main.DesktopScheduler;
import models.Appointment;
import util.Helper;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.ResourceBundle;

public class ReportAppointmentTypeController implements Initializable {

    // Components
    @FXML private ChoiceBox<String> reportChoiceBox;
    @FXML private Label monthYearLabel;
    @FXML private Text numberOfTypesText;
    @FXML private Text numberOfAppointmentsText;

    // Reports Table
    @FXML private TableView<Map.Entry<String, Integer>> reportTableView;
    @FXML private TableColumn<Map.Entry<String, Integer>, String> typeTableColumn;
    @FXML private TableColumn<Map.Entry<String, Integer>, Integer> occurrencesTableColumn;

    // Java Calendar Instance
    private final Calendar calendar = Calendar.getInstance();

    // Date Formats
    private final SimpleDateFormat monthYearFormat = new SimpleDateFormat("MMMM yyyy");
    private final SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyy-MM");

    // Report Data
    ObservableMap<String, Integer> appointmentTypeHashMap = FXCollections.observableHashMap();

    private void generateReportTableData() {

        // Counters
        int appointmentTypeCount = 0, appointmentCount = 0;

        // Remove previous data
        appointmentTypeHashMap.clear();

        // Get all appointments for the current month
        ObservableList<Appointment> currentAppointments = DesktopScheduler.getLocalData()
                .getAppointmentsByMonth(yearMonthFormat.format(calendar.getTime()), DesktopScheduler.getCurrentUser().getUserName());

        // Add data to the appointmentTypeHashMap
        for (String type:DesktopScheduler.getLocalData().getAppointmentTypes()) {

            // Number of appointments by type
            int numberOfAppointments = DesktopScheduler.getLocalData()
                    .getAppointmentCountByType(currentAppointments, type);

            if (numberOfAppointments > 0) {
                appointmentTypeHashMap.put(type, numberOfAppointments);
                appointmentCount += numberOfAppointments;
                appointmentTypeCount++;
            }
        }

        // Populate the table with the keys and values of the HashMap
        ObservableList<Map.Entry<String, Integer>> items = FXCollections.observableArrayList(appointmentTypeHashMap.entrySet());
        reportTableView.setItems(items);

        // Change the value of the statistics
        numberOfTypesText.setText(Integer.toString(appointmentTypeCount));
        numberOfAppointmentsText.setText(Integer.toString(appointmentCount));
    }

    // Navigation Controls
    @FXML private void navbarAppointmentsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("AppointmentsMonthly");
    }
    @FXML private void navbarCustomersButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("Customers");
    }
    @FXML private void navbarLogOutButton(ActionEvent event) throws IOException {
        System.out.println("Logged out\n");
        DesktopScheduler.changeScenes("LogIn");
    }

    // Month Controls
    @FXML private void previousMonthButton(ActionEvent event) {
        calendar.add(Calendar.MONTH, -1);
        monthYearLabel.setText(monthYearFormat.format(calendar.getTime()));
        generateReportTableData();
    }
    @FXML private void nextMonthButton(ActionEvent event) {
        calendar.add(Calendar.MONTH, 1);
        monthYearLabel.setText(monthYearFormat.format(calendar.getTime()));
        generateReportTableData();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Configure Report ChoiceBox
        reportChoiceBox.setItems(DesktopScheduler.getLocalData().getReportTypes());
        reportChoiceBox.setValue("Number of appointment types by month");
        reportChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.equals("Schedule for each consultant")) {
                    try {
                        DesktopScheduler.changeScenes("ReportConsultantSchedule");
                    } catch (IOException e) {
                        System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
                    }
                } else if (newValue.equals("Hours worked for each consultant")) {
                    try {
                        DesktopScheduler.changeScenes("ReportConsultantHours");
                    } catch (IOException e) {
                        System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
                    }
                }
            }
        });

        // Configure monthYearLabel
        monthYearLabel.setText(monthYearFormat.format(calendar.getTime()));

        // Configure Report Table
        generateReportTableData();

        typeTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, Integer>, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Map.Entry<String, Integer>, String> param) {
                return new SimpleStringProperty(param.getValue().getKey());
            }
        });
        occurrencesTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, Integer>, Integer>, ObservableValue<Integer>>() {
            @Override
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Map.Entry<String, Integer>, Integer> param) {
                return new SimpleIntegerProperty(param.getValue().getValue()).asObject();
            }
        });
    }
}

//*******************************************************************
// ReportConsultantHoursController
//
// Controls the interactions for ReportConsultantHours.fxml. This shows the accumulated amount of time spent in
// appointments for each user per month.
//*******************************************************************

package controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import main.DesktopScheduler;
import models.Appointment;
import models.User;
import util.Helper;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Map;
import java.util.ResourceBundle;

public class ReportConsultantHoursController implements Initializable {

    // Components
    @FXML private ChoiceBox<String> reportChoiceBox;
    @FXML private Label monthYearLabel;

    // Report Table
    @FXML private TableView<Map.Entry<String, String>> reportTableView;
    @FXML private TableColumn<Map.Entry<String, String>, String> consultantTableColumn;
    @FXML private TableColumn<Map.Entry<String, String>, String> hoursTableColumn;

    // Java Calendar Instance
    private final Calendar calendar = Calendar.getInstance();

    // Date Formats
    private final SimpleDateFormat monthYearFormat = new SimpleDateFormat("MMMM yyyy");
    private final SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyy-MM");

    // Report Data
    ObservableMap<String, String> consultantHoursHashMap = FXCollections.observableHashMap();

    private void generateReportTableData() {

        // Remove previous data
        consultantHoursHashMap.clear();

        // Add data to the consultantHoursHashMap
        for (User user:DesktopScheduler.getLocalData().getAllUsers()) {

            // Time accumulated per contact
            long accumulatedHours = 0, accumulatedMinutes = 0;

            for (Appointment appointment:DesktopScheduler.getLocalData().getAppointmentsByMonth(yearMonthFormat
                    .format(calendar.getTime()), user.getUserName())) {

                // Calculate the time between
                LocalDateTime tempDateTime = LocalDateTime.from(appointment.getStart());
                long hours = tempDateTime.until(appointment.getEnd(), ChronoUnit.HOURS);
                tempDateTime = tempDateTime.plusHours( hours );
                long minutes = tempDateTime.until(appointment.getEnd(), ChronoUnit.MINUTES);

                accumulatedHours += hours;
                accumulatedMinutes += minutes;
            }

            // Add the data to the HashMap
            consultantHoursHashMap.put(user.getUserName(),
                    accumulatedHours + " hours and " + accumulatedMinutes + " minutes");
        }

        // Populate the table with the keys and values of the HashMap
        ObservableList<Map.Entry<String, String>> items = FXCollections.observableArrayList(consultantHoursHashMap.entrySet());
        reportTableView.setItems(items);
    }

    // Navigation Controls
    @FXML private void navbarAppointmentsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("AppointmentsMonthly");
    }
    @FXML private void navbarCustomersButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("Customers");
    }
    @FXML private void navbarLogOutButton(ActionEvent event) throws IOException {
        System.out.println("Logged out\n");
        DesktopScheduler.changeScenes("LogIn");
    }

    // Month Controls
    @FXML private void previousMonthButton(ActionEvent event) {
        calendar.add(Calendar.MONTH, -1);
        monthYearLabel.setText(monthYearFormat.format(calendar.getTime()));
        generateReportTableData();
    }
    @FXML private void nextMonthButton(ActionEvent event) {
        calendar.add(Calendar.MONTH, 1);
        monthYearLabel.setText(monthYearFormat.format(calendar.getTime()));
        generateReportTableData();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Configure Report ChoiceBox
        reportChoiceBox.setItems(DesktopScheduler.getLocalData().getReportTypes());
        reportChoiceBox.setValue("Hours worked for each consultant");
        reportChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.equals("Number of appointment types by month")) {
                    try {
                        DesktopScheduler.changeScenes("ReportAppointmentType");
                    } catch (IOException e) {
                        System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
                    }
                } else if (newValue.equals("Schedule for each consultant")) {
                    try {
                        DesktopScheduler.changeScenes("ReportConsultantSchedule");
                    } catch (IOException e) {
                        System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
                    }
                }
            }
        });

        // Configure monthYearLabel
        monthYearLabel.setText(monthYearFormat.format(calendar.getTime()));

        // Configure Report Table
        generateReportTableData();

        // Configure Report Table
        consultantTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, String>, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Map.Entry<String, String>, String> param) {
                return new SimpleStringProperty(param.getValue().getKey());
            }
        });
        hoursTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Map.Entry<String, String>, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Map.Entry<String, String>, String> param) {
                return new SimpleStringProperty(param.getValue().getValue());
            }
        });

    }
}

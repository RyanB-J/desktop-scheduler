//*******************************************************************
// ReportConsultantScheduleController
//
// Controls the interactions for ReportConsultantSchedule.fxml. This provides a nicer format to view all of the
// appointments for each user per month.
//*******************************************************************

package controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import main.DesktopScheduler;
import models.Appointment;
import util.Helper;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.ResourceBundle;

public class ReportConsultantScheduleController implements Initializable {

    // Components
    @FXML private ChoiceBox<String> reportChoiceBox;
    @FXML private ChoiceBox<String> consultantChoiceBox;
    @FXML private Label monthYearLabel;

    // Report Table
    @FXML private TableView<Appointment> reportTableView;
    @FXML private TableColumn<Appointment, String> appointmentTableColumn;
    @FXML private TableColumn<Appointment, String> dateTableColumn;
    @FXML private TableColumn<Appointment, String> timeTableColumn;

    // Java Calendar Instance
    private final Calendar calendar = Calendar.getInstance();

    // Date Formatters
    private final SimpleDateFormat monthYearFormat = new SimpleDateFormat("MMMM yyyy");
    private final SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyy-MM");
    
    // Report Table Items
    private final ObservableList<Appointment> reportTableItems = FXCollections.observableArrayList();
    
    private void generateReportTableData() {

        // Clear the table of existing appointments
        reportTableItems.clear();
        reportTableView.setItems(reportTableItems);
        
        // Populate all of the appointments for the user for that month
        reportTableItems.addAll(DesktopScheduler.getLocalData().getAppointmentsByMonth(
                yearMonthFormat.format(calendar.getTime()), consultantChoiceBox.getValue()));
        reportTableView.setItems(reportTableItems);
    }

    // Navigation Controls
    @FXML private void navbarAppointmentsButton(javafx.event.ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("AppointmentsMonthly");
    }
    @FXML private void navbarCustomersButton(javafx.event.ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("Customers");
    }
    @FXML private void navbarLogOutButton(javafx.event.ActionEvent event) throws IOException {
        System.out.println("Logged out\n");
        DesktopScheduler.changeScenes("LogIn");
    }

    // Month Controls
    @FXML private void previousMonthButton(ActionEvent event) {
        calendar.add(Calendar.MONTH, -1);
        monthYearLabel.setText(monthYearFormat.format(calendar.getTime()));
        generateReportTableData();
    }
    @FXML private void nextMonthButton(ActionEvent event) {
        calendar.add(Calendar.MONTH, 1);
        monthYearLabel.setText(monthYearFormat.format(calendar.getTime()));
        generateReportTableData();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Configure reportChoiceBox
        reportChoiceBox.setItems(DesktopScheduler.getLocalData().getReportTypes());
        reportChoiceBox.setValue("Schedule for each consultant");
        reportChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.equals("Number of appointment types by month")) {
                    try {
                        DesktopScheduler.changeScenes("ReportAppointmentType");
                    } catch (IOException e) {
                        System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
                    }
                } else if (newValue.equals("Hours worked for each consultant")) {
                    try {
                        DesktopScheduler.changeScenes("ReportConsultantHours");
                    } catch (IOException e) {
                        System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
                    }
                }
            }
        });

        // Configure consultantChoiceBox
        consultantChoiceBox.setItems(DesktopScheduler.getLocalData().getAllUsersByName());
        consultantChoiceBox.setValue(consultantChoiceBox.getItems().get(0));
        consultantChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                generateReportTableData();
            }
        });

        // Configure monthYearLabel
        monthYearLabel.setText(monthYearFormat.format(calendar.getTime()));

        // Configure Report Table
        appointmentTableColumn.setCellValueFactory(new PropertyValueFactory<Appointment, String>("title"));
        dateTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Appointment, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Appointment, String> param) {
                return new SimpleStringProperty(param.getValue().getStart().format(DateTimeFormatter.ofPattern("MMMM d")));
            }
        });
        timeTableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Appointment, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Appointment, String> param) {
                return new SimpleStringProperty(param.getValue().getStart().format(DateTimeFormatter.ofPattern("h:mm a")));
            }
        });
        generateReportTableData();

    }
}

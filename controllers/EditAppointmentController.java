//*******************************************************************
// EditAppointmentController
//
// Controls the interactions for EditAppointment.fxml. This is used to modify an existing appointment in the application.
//*******************************************************************

package controllers;

import exceptions.AppointmentConflictException;
import exceptions.InvalidDataException;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.DesktopScheduler;
import models.Appointment;
import models.Customer;
import util.Database;
import util.Helper;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class EditAppointmentController implements Initializable {

    // Components
    @FXML private TextField titleTextField;
    @FXML private ChoiceBox<String> typeChoiceBox;
    @FXML private TextArea descriptionTextArea;
    @FXML private TextField urlTextField;
    @FXML private ChoiceBox<String> customerChoiceBox;
    @FXML private TextField locationTextField;
    @FXML private DatePicker dateDatePicker;
    @FXML private ComboBox<String> startTimeComboBox;
    @FXML private ComboBox<String> endTimeComboBox;
    @FXML private Label businessHoursLabel;

    // State Variables
    private final Appointment selectedAppointment = AppointmentsMonthlyController.getSelectedAppointment();
    private static String lastScene = "AppointmentsMonthly";
    private Customer selectedCustomer;

    // Setters
    public static void setLastScene(String currentScene) { lastScene = currentScene; }

    // Date Formatters
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd h:mm a");

    // Navigation Controls
    @FXML private void navbarCustomersButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("Customers");
    }
    @FXML private void navbarReportsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("ReportAppointmentType");
    }
    @FXML private void navbarLogOutButton(ActionEvent event) throws IOException {
        System.out.println("Logged out\n");
        DesktopScheduler.changeScenes("LogIn");
    }

    // Alerts
    private final Alert customerActiveAlert = new Alert(Alert.AlertType.ERROR,
            "Customer account not active.", ButtonType.CLOSE);
    private final Alert timeDurationAlert = new Alert(Alert.AlertType.ERROR,
            "Time duration is invalid.", ButtonType.CLOSE);
    private final Alert businessHoursAlert = new Alert(Alert.AlertType.ERROR,
            "Appointment is outside of business hours.", ButtonType.CLOSE);

    private void goToPreviousScene() {
        if (lastScene.equals("AppointmentsMonthly")) {
            try {
                DesktopScheduler.changeScenes("AppointmentsMonthly");
            } catch (IOException e) {
                System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
            }
        } else if (lastScene.equals("AppointmentsWeekly")) {
            try {
                DesktopScheduler.changeScenes("AppointmentsWeekly");
            } catch (IOException e) {
                System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
            }
        }
    }

    // Validate Form
    private boolean formValidated() {

        // Is customer active
        if (!selectedCustomer.isActive()) {
            customerActiveAlert.showAndWait();
            return false;
        }

        // Is endTime before startTime
        else if (endTimeComboBox.getSelectionModel().getSelectedIndex()
                <= startTimeComboBox.getSelectionModel().getSelectedIndex()) {
            timeDurationAlert.showAndWait();
            return false;
        }

        // Is the appointment within business hours
        ObservableList<String> timeIncrements = DesktopScheduler.getLocalData().getTimeIncrements();
        int openTimeIndex = timeIncrements.indexOf(DesktopScheduler.getLocalData().openingTime);
        int closeTimeIndex = timeIncrements.indexOf(DesktopScheduler.getLocalData().closingTime);
        int startTimeIndex = timeIncrements.indexOf(startTimeComboBox.getValue());
        int endTimeIndex = timeIncrements.indexOf(endTimeComboBox.getValue());
        if (startTimeIndex < openTimeIndex || endTimeIndex > closeTimeIndex) {
            businessHoursAlert.showAndWait();
            throw new InvalidDataException("Start Time and/or End Time are outside of Business Hours.");
        }

        // Is the appointment clashing with another
        LocalDateTime startLocalDateTime = LocalDateTime.parse(dateDatePicker.getValue() + " "
                + startTimeComboBox.getValue(), dateTimeFormatter);
        LocalDateTime endLocalDateTime = LocalDateTime.parse(dateDatePicker.getValue() + " "
                + endTimeComboBox.getValue(), dateTimeFormatter);

        for (Appointment appointment:DesktopScheduler.getLocalData().getAppointmentsByDate(dateDatePicker.getValue().toString())) {
            if (appointment.getId() != selectedAppointment.getId()) {
                if (!(startLocalDateTime.isAfter(appointment.getEnd()) || endLocalDateTime.isBefore(appointment.getStart()))) {
                    throw new AppointmentConflictException("Appointment is conflicting with an existing appointment.");
                }
            }
        }

        return true;
    }

    // Save the Appointment
    @FXML private void saveButton(ActionEvent event) {

        // Get the selected customer
        selectedCustomer = DesktopScheduler.getLocalData().getAllCustomers()
                .get(customerChoiceBox.getSelectionModel().getSelectedIndex());

        if (formValidated()) {

            System.out.println(Helper.changeColor("Updating Appointment...", Helper.Color.YELLOW));

            // Create LocalDateTimes for the new appointment object
            LocalDateTime startLocalDateTime = LocalDateTime.parse(dateDatePicker.getValue() + " "
                    + startTimeComboBox.getValue(), dateTimeFormatter);
            LocalDateTime endLocalDateTime = LocalDateTime.parse(dateDatePicker.getValue() + " "
                    + endTimeComboBox.getValue(), dateTimeFormatter);

            // Convert the dateTime to UTC
            LocalDateTime utcStartDateTime = (startLocalDateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime());
            LocalDateTime utcEndDateTime = (endLocalDateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime());

            String appointmentUpdate = "UPDATE `appointment`"
                    + " SET `customerId` = ?, `title` = ?, `description` = ?, `location` = ?, `type` = ?, `url` = ?,"
                    + " `start` = ?, `end` = ?"
                    + " WHERE `appointmentId` = ?";
            try (PreparedStatement updateAppointment = Database.getConnection().prepareStatement(appointmentUpdate)) {
                updateAppointment.setInt(1, selectedCustomer.getId());
                updateAppointment.setString(2, Helper.toTitleCase(titleTextField.getText()));
                updateAppointment.setString(3, descriptionTextArea.getText());
                updateAppointment.setString(4, Helper.toTitleCase(locationTextField.getText()));
                updateAppointment.setString(5, typeChoiceBox.getValue());
                updateAppointment.setString(6, urlTextField.getText());
                updateAppointment.setTimestamp(7, Timestamp.valueOf(utcStartDateTime));
                updateAppointment.setTimestamp(8, Timestamp.valueOf(utcEndDateTime));
                updateAppointment.setInt(9, selectedAppointment.getId());

                // Update the record
                int rowsAffected = updateAppointment.executeUpdate();
                System.out.println("\tUpdated Appointment: " + rowsAffected + " row affected.");
                if (rowsAffected == 0) System.out.println("\tAppointment Update Failed");

                // Update the existing appointment object
                selectedAppointment.setCustomerId(selectedCustomer.getId());
                selectedAppointment.setTitle(Helper.toTitleCase(titleTextField.getText()));
                selectedAppointment.setDescription(descriptionTextArea.getText());
                selectedAppointment.setLocation(Helper.toTitleCase(locationTextField.getText()));
                selectedAppointment.setType(typeChoiceBox.getValue());
                selectedAppointment.setUrl(urlTextField.getText());
                selectedAppointment.setStart(startLocalDateTime);
                selectedAppointment.setEnd(endLocalDateTime);

                System.out.println(Helper.changeColor("Appointment Updated", Helper.Color.GREEN) + "\n");

            } catch (SQLException e) {
                System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
            }

            goToPreviousScene();
        }
    }

    // Cancel the Form
    @FXML private void cancelButton(ActionEvent event) {
        goToPreviousScene();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Configure Text Fields
        titleTextField.setText(selectedAppointment.getTitle());
        descriptionTextArea.setText(selectedAppointment.getDescription());
        urlTextField.setText(selectedAppointment.getUrl());
        locationTextField.setText(selectedAppointment.getLocation());

        // Configure Type Choice Box
        typeChoiceBox.setItems(DesktopScheduler.getLocalData().getAppointmentTypes());
        typeChoiceBox.setValue(selectedAppointment.getType());

        // Configure Customer Choice Box
        customerChoiceBox.setItems(DesktopScheduler.getLocalData().getAllCustomersByName());
        customerChoiceBox.setValue(DesktopScheduler.getLocalData().getCustomerById(
                selectedAppointment.getCustomerId()).getCustomerName());

        // Configure Date Picker
        dateDatePicker.setValue(selectedAppointment.getStart().toLocalDate());

        // Configure Time Choice Box
        startTimeComboBox.setItems(DesktopScheduler.getLocalData().getTimeIncrements());
        startTimeComboBox.setValue(selectedAppointment.getStart().format(DateTimeFormatter.ofPattern("h:mm a")));
        endTimeComboBox.setItems(DesktopScheduler.getLocalData().getTimeIncrements());
        endTimeComboBox.setValue(selectedAppointment.getEnd().format(DateTimeFormatter.ofPattern("h:mm a")));

        // Configure Business Hours Label
        businessHoursLabel.setText(DesktopScheduler.getLocalData().openingTime
                + " - " + DesktopScheduler.getLocalData().closingTime);
    }
}

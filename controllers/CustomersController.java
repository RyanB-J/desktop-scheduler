//*******************************************************************
// Customers Controller
//
// Controls the interactions for Customers.fxml. This shows all of the customers in the application and allows the user
// to modify them.
//*******************************************************************

package controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import main.DesktopScheduler;
import models.Customer;
import util.Database;
import util.Helper;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class CustomersController implements Initializable {

    // Components
    @FXML private Button searchButton;
    @FXML private Button deleteButton;
    @FXML private Button editButton;
    @FXML private TextField searchTextField;

    // Customer table
    @FXML private TableView<Customer> customerTableView;
    @FXML private TableColumn<Customer, String> customerNameColumn;
    @FXML private TableColumn<Customer, String> customerAddressColumn;
    @FXML private TableColumn<Customer, String> customerPhoneColumn;

    // Table Selection
    public static Customer selectedCustomer;

    // Search TextField Listener
    @FXML private void searchTextFieldKeyPressed(KeyEvent event) {
        if (event.getCode().toString().equals("ENTER")) {
            searchButton.fire();
        }
    }

    // Delete Customer Alert
    private static final Alert deleteAlert = new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure? This action can not be undone.",
            ButtonType.YES, ButtonType.NO);

    // Navigation Controls
    @FXML private void navbarAppointmentsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("AppointmentsMonthly");
    }
    @FXML private void navbarReportsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("ReportAppointmentType");
    }
    @FXML private void navbarLogOutButton(ActionEvent event) throws IOException {
        System.out.println("Logged out\n");
        DesktopScheduler.changeScenes("LogIn");
    }

    // Add a Customer
    @FXML private void addButton(ActionEvent event) {
        try {
            DesktopScheduler.changeScenes("AddCustomer");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    // Search for a Customer
    @FXML private void searchButton(ActionEvent event) {
        String query = searchTextField.getText();
        ObservableList<Customer> queryMatches;

        if (query != null && !query.isEmpty()) {

            // If query contains only numbers, search by phone number
            if (query.matches("^[\\d]+$")) {
                queryMatches = DesktopScheduler.getLocalData().searchCustomers(query, "phone");
            }

            // If query contains only letters or symbols, search by customerName
            else if (query.matches("^[\\D]+$")) {
                queryMatches = DesktopScheduler.getLocalData().searchCustomers(query, "customerName");
            }

            // Otherwise, search by address
            else {
                queryMatches = DesktopScheduler.getLocalData().searchCustomers(query, "address");
            }

            // Change the customer table to show query matches
            customerTableView.setItems(queryMatches);

        } else {

            // Change the customer table to show all customers
            customerTableView.setItems(DesktopScheduler.getLocalData().getAllCustomers());
        }
    }

    // Edit a selected Customer
    @FXML private void editButton(ActionEvent event) {

        try {
            DesktopScheduler.changeScenes("EditCustomer");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    // Delete a selected Customer
    @FXML private void deleteButton(ActionEvent event) {
        deleteAlert.showAndWait();
        if (deleteAlert.getResult() == ButtonType.YES) {

            System.out.println(Helper.changeColor("Deleting Customer...", Helper.Color.YELLOW));

            String customerDelete = "DELETE FROM `customer` WHERE `customerId` = ?";
            try (PreparedStatement deleteCustomer = Database.getConnection().prepareStatement(customerDelete)) {
                deleteCustomer.setInt(1, selectedCustomer.getId());

                // Delete the record
                int rowsAffected = deleteCustomer.executeUpdate();
                System.out.println("\tRemoved Customer: " + rowsAffected + " row affected.");
                if (rowsAffected == 0) System.out.println("\tCustomer Deletion Failed");

                // Update customer array
                DesktopScheduler.getLocalData().deleteCustomer(selectedCustomer);

                // Update the customer table
                customerTableView.getSelectionModel().select(null);
                editButton.setDisable(true);
                deleteButton.setDisable(true);

                System.out.println(Helper.changeColor("Customer deleted", Helper.Color.GREEN));
                System.out.println(DesktopScheduler.getLocalData().getAllCustomers().size() + " customers in memory\n");

            } catch (SQLException e) {
                System.out.println("\t" + Helper.changeColor("Exception: " + e.getMessage(), Helper.Color.RED));
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Set Edit and Delete buttons to disabled
        editButton.setDisable(true);
        deleteButton.setDisable(true);

        // Initialize columns in the customer table
        customerNameColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("customerName"));
        customerAddressColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("address1"));
        customerPhoneColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("phone"));
        customerTableView.setItems(DesktopScheduler.getLocalData().getAllCustomers());

        // Customer Table Selection Listener
        customerTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Customer>() {
            @Override
            public void changed(ObservableValue<? extends Customer> observable, Customer oldValue, Customer newValue) {
                if (customerTableView.getSelectionModel().getSelectedItem() != null) {
                    selectedCustomer = newValue;
                    editButton.setDisable(false);
                    deleteButton.setDisable(false);
                }
            }
        });

    }
}

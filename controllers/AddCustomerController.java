//*******************************************************************
// AddCustomerController
//
// Controls the interactions for Customers.fxml. This is used to add customers to the application.
//*******************************************************************

package controllers;

import exceptions.InvalidDataException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.DesktopScheduler;
import models.Customer;
import util.Database;
import util.Helper;
import util.Validation;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ResourceBundle;

public class AddCustomerController implements Initializable {

    // Components
    @FXML private TextField nameTextField;
    @FXML private TextField address1TextField;
    @FXML private TextField address2TextField;
    @FXML private TextField cityTextField;
    @FXML private TextField zipTextField;
    @FXML private ChoiceBox<String> countryChoiceBox;
    @FXML private TextField phoneTextField;
    @FXML private CheckBox activeCheckBox;

    // State Variables
    private boolean isActive = true;

    private final Alert invalidDataAlert = new Alert(Alert.AlertType.ERROR,
            "One or more of the required form fields are empty.", ButtonType.CLOSE);

    private boolean checkFormFields() {

        // Reset the fields
        nameTextField.setStyle("-fx-border-color: inherit");
        address1TextField.setStyle("-fx-border-color: inherit");
        cityTextField.setStyle("-fx-border-color: inherit");
        zipTextField.setStyle("-fx-border-color: inherit");
        phoneTextField.setStyle("-fx-border-color: inherit");

        // Check if important fields are empty
        boolean isError = false;
        if (nameTextField.getText().isEmpty()) {
            nameTextField.setStyle("-fx-border-color: red");
            isError = true;
        }
        if (address1TextField.getText().isEmpty()) {
            address1TextField.setStyle("-fx-border-color: red");
            isError = true;
        }
        if (cityTextField.getText().isEmpty()) {
            cityTextField.setStyle("-fx-border-color: red");
            isError = true;
        }
        if (zipTextField.getText().isEmpty()) {
            zipTextField.setStyle("-fx-border-color: red");
            isError = true;
        }
        if (phoneTextField.getText().isEmpty()) {
            phoneTextField.setStyle("-fx-border-color: red");
            isError = true;
        }
        return !isError;
    }

    // Active Checkbox
    @FXML private void activeCheckBox(ActionEvent event) {
        isActive = activeCheckBox.isSelected();
    }

    // Navigation Controls
    @FXML private void navbarAppointmentsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("AppointmentsMonthly");
    }
    @FXML private void navbarReportsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("ReportAppointmentType");
    }
    @FXML private void navbarLogOutButton(ActionEvent event) throws IOException {
        System.out.println("Logged out\n");
        DesktopScheduler.changeScenes("LogIn");
    }

    // Add a Customer
    @FXML private void addButton(ActionEvent event) {

        System.out.println(Helper.changeColor("Creating a Customer...", Helper.Color.YELLOW));

        // Check if form is valid
        if (checkFormFields()) {

            // Temporary state variables
            int cityID = 0;
            int addressID = 0;

            // Static DateTime for following statements
            LocalDateTime utcCurrentDateTime = (LocalDateTime.now().atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime());
            ;

            // Create a Customer Object
            Customer newCustomer = new Customer();

            // Add the country to the new customer object
            newCustomer.setCountry(countryChoiceBox.getValue());

            // Check if city already exists
            String cityQuery = "SELECT `cityId`, `city` FROM `city` WHERE `city` = LOWER(?)";
            try (PreparedStatement getCity = Database.getConnection().prepareStatement(cityQuery)) {
                getCity.setString(1, cityTextField.getText().toLowerCase());

                try (ResultSet results = getCity.executeQuery()) {

                    // If the city doesn't exist
                    if (!results.next()) {

                        // Add the city to the database
                        String cityInsert = "INSERT INTO `city`"
                                + " (`city`, `countryId`, `createDate`, `createdBy`, `lastUpdateBy`)"
                                + " VALUES (?, ?, ?, ?, ?)";
                        try (PreparedStatement createCity = Database.getConnection().prepareStatement(cityInsert,
                                Statement.RETURN_GENERATED_KEYS)) {
                            createCity.setString(1, Helper.toTitleCase(cityTextField.getText()));
                            createCity.setInt(2, DesktopScheduler.getLocalData().getCountryId(countryChoiceBox.getValue()));
                            createCity.setObject(3, Timestamp.valueOf(utcCurrentDateTime));
                            createCity.setString(4, DesktopScheduler.getCurrentUser().getUserName());
                            createCity.setString(5, DesktopScheduler.getCurrentUser().getUserName());

                            // Insert the record
                            int rowsAffected = createCity.executeUpdate();
                            System.out.println("\tCreated City: " + rowsAffected + " row affected.");
                            if (rowsAffected == 0) System.out.println("\tCity Creation Failed");

                            // Get the key of the insert
                            try (ResultSet generatedKey = createCity.getGeneratedKeys()) {
                                if (generatedKey.next()) {
                                    cityID = generatedKey.getInt(1);
                                    newCustomer.setCityId(cityID);
                                }
                            }

                            // Add to the new customer object
                            newCustomer.setCity(Helper.toTitleCase(cityTextField.getText()));

                        } catch (SQLException e) {
                            System.out.println("\t" + Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
                        }

                    } else results.beforeFirst();

                    // If the city does exist
                    while (results.next()) {

                        // Set the city id to the existing city
                        cityID = results.getInt(1);
                        newCustomer.setCityId(cityID);
                        System.out.println("\tUsing city: " + results.getString(2));

                        // Add to the new customer object
                        newCustomer.setCity(results.getString(2));
                    }
                }

            } catch (SQLException e) {
                System.out.println("\t" + Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
            }

            // Add the address to the database
            String addressInsert = "INSERT INTO `address`"
                    + " (`address`, `address2`, `cityId`, `postalCode`, `phone`, `createDate`, `createdBy`, `lastUpdateBy`)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            try (PreparedStatement createAddress = Database.getConnection().prepareStatement(addressInsert,
                    Statement.RETURN_GENERATED_KEYS)) {
                createAddress.setString(1, Helper.toTitleCase(address1TextField.getText()));
                createAddress.setString(2, Helper.toTitleCase(address2TextField.getText()));
                createAddress.setInt(3, cityID);
                createAddress.setString(4, zipTextField.getText());
                createAddress.setString(5, phoneTextField.getText());
                createAddress.setObject(6, Timestamp.valueOf(utcCurrentDateTime));
                createAddress.setString(7, DesktopScheduler.getCurrentUser().getUserName());
                createAddress.setString(8, DesktopScheduler.getCurrentUser().getUserName());

                // Insert the record
                int rowsAffected = createAddress.executeUpdate();
                System.out.println("\tCreated Address: " + rowsAffected + " row affected.");
                if (rowsAffected == 0) System.out.println("\tAddress Creation Failed");

                // Get the key of the insert
                try (ResultSet generatedKey = createAddress.getGeneratedKeys()) {
                    if (generatedKey.next()) {
                        addressID = generatedKey.getInt(1);
                        newCustomer.setAddressId(addressID);
                    }
                }

                // Add to the new customer object
                newCustomer.setAddress1(Helper.toTitleCase(address1TextField.getText()));
                newCustomer.setAddress2(Helper.toTitleCase(address2TextField.getText()));
                newCustomer.setPostalCode(zipTextField.getText());
                newCustomer.setPhone(phoneTextField.getText());

            } catch (SQLException e) {
                System.out.println("\t" + Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
            }

            // Add the customer to the database
            String customerInsert = "INSERT INTO `customer`"
                    + " (`customerName`, `addressId`, `active`, `createDate`, `createdBy`, `lastUpdateBy`)"
                    + " VALUES (?, ?, ?, ?, ?, ?)";
            try (PreparedStatement createCustomer = Database.getConnection().prepareStatement(customerInsert,
                    Statement.RETURN_GENERATED_KEYS)) {
                createCustomer.setString(1, Helper.toTitleCase(nameTextField.getText()));
                createCustomer.setInt(2, addressID);
                createCustomer.setInt(3, (isActive ? 1 : 0));
                createCustomer.setObject(4, Timestamp.valueOf(utcCurrentDateTime));
                createCustomer.setString(5, DesktopScheduler.getCurrentUser().getUserName());
                createCustomer.setString(6, DesktopScheduler.getCurrentUser().getUserName());

                // Insert the record
                int rowsAffected = createCustomer.executeUpdate();
                System.out.println("\tCreated Customer: " + rowsAffected + " row affected.");
                if (rowsAffected == 0) System.out.println("\tCustomer Creation Failed");

                // Get the key of the insert
                try (ResultSet generatedKey = createCustomer.getGeneratedKeys()) {
                    if (generatedKey.next()) {
                        newCustomer.setId(generatedKey.getInt(1));
                    }
                }

                // Add to the new customer object
                newCustomer.setCustomerName(Helper.toTitleCase(nameTextField.getText()));
                newCustomer.setActive(isActive);

            } catch (SQLException e) {
                System.out.println("\t" + Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
            }

            System.out.println(Helper.changeColor("Customer created", Helper.Color.GREEN));

            // Add new customer to memory
            DesktopScheduler.getLocalData().addCustomer(newCustomer);
            System.out.println(DesktopScheduler.getLocalData().getAllCustomers().size() + " customers in memory\n");

            // Go back to Customers screen
            try {
                DesktopScheduler.changeScenes("Customers");
            } catch (IOException e) {
                System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
            }

        } else {
            System.out.println(Helper.changeColor("Customer creation failed. Invalid data submitted.", Helper.Color.RED));
            invalidDataAlert.showAndWait();
            throw new InvalidDataException("Invalid Customer data was submitted.");
        }

    }

    // Cancel the form
    @FXML private void cancelButton(ActionEvent event) {
        try {
            DesktopScheduler.changeScenes("Customers");
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Configure the ChoiceBox
        countryChoiceBox.setItems(DesktopScheduler.getLocalData().getCountries());
        countryChoiceBox.setValue(countryChoiceBox.getItems().get(0));

        // Set max lengths to Text Fields
        Validation.formatTextField(nameTextField, 45, false);
        Validation.formatTextField(address1TextField, 50, false);
        Validation.formatTextField(address2TextField, 50, false);
        Validation.formatTextField(cityTextField, 50, false);
        Validation.formatTextField(zipTextField, 10, true);
        Validation.formatTextField(phoneTextField, 20, true);
    }
}


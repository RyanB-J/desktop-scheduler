//*******************************************************************
// AppointmentsWeeklyController
//
// Controls the interactions for AppointmentsWeekly.fxml. This shows appointments on an interactive weekly calendar
// (based on who is logged in) and allows the user to modify them.
//*******************************************************************

package controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import main.DesktopScheduler;
import models.Appointment;
import util.Database;
import util.Helper;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class AppointmentsWeeklyController implements Initializable {

    // Components
    @FXML private Label calendarTitleLabel;
    @FXML private Button editButton;
    @FXML private Button deleteButton;

    // Tables
    @FXML private TableView<Appointment> sundayTableView;
    @FXML private TableView<Appointment> mondayTableView;
    @FXML private TableView<Appointment> tuesdayTableView;
    @FXML private TableView<Appointment> wednesdayTableView;
    @FXML private TableView<Appointment> thursdayTableView;
    @FXML private TableView<Appointment> fridayTableView;
    @FXML private TableView<Appointment> saturdayTableView;
    @FXML private TableColumn<Appointment, String> sundayTableColumn;
    @FXML private TableColumn<Appointment, String> mondayTableColumn;
    @FXML private TableColumn<Appointment, String> tuesdayTableColumn;
    @FXML private TableColumn<Appointment, String> wednesdayTableColumn;
    @FXML private TableColumn<Appointment, String> thursdayTableColumn;
    @FXML private TableColumn<Appointment, String> fridayTableColumn;
    @FXML private TableColumn<Appointment, String> saturdayTableColumn;

    // Java Calendar Instance
    private final Calendar calendar = Calendar.getInstance();

    // Date Formats
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM d yyyy");
    private final SimpleDateFormat monthDayFormat = new SimpleDateFormat("MMM d");
    private final SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final SimpleDateFormat isoSingleDateFormat = new SimpleDateFormat("yyyy-MM-d");
    private final DateTimeFormatter isoDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    // LinkedHashMap for creating an iterable relationship between two JavaFX components
    private final LinkedHashMap<TableView<Appointment>, TableColumn<Appointment, String>> calendarHashMap = new LinkedHashMap<>();

    // Local Variables
    private final Set<TableView<Appointment>> calendarHashMapSet = calendarHashMap.keySet();

    // Delete Customer Alert
    private static final Alert deleteAlert = new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure? This action can not be undone.",
            ButtonType.YES, ButtonType.NO);

    // Generate the Calendar for the Week
    private void generateCalendar() {

        // Set calendar to Sunday and configure title text
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        String calendarTitleText = dateFormat.format(calendar.getTime()) + " - ";

        // Iterate through the calendarHashMap to populate the tables
        for (TableView<Appointment> tableView:calendarHashMapSet) {

            // Clear any existing data
            tableView.getItems().clear();

            // Change the table header text to the calendar day
            calendarHashMap.get(tableView).setText(monthDayFormat.format(calendar.getTime()));

            // Populate the table if any appointments exist
            ObservableList<Appointment> currentAppointments =
                    DesktopScheduler.getLocalData().getAppointmentsByDate(isoSingleDateFormat.format(calendar.getTime()));
            if (!currentAppointments.isEmpty()) {
                tableView.setItems(currentAppointments);
            }

            // Check if this day is today's date (Inoperative)
            if (isoDateFormat.format(calendar.getTime()).equals(LocalDateTime.now().format(isoDateFormatter))) {
                if (!calendarHashMap.get(tableView).getStyleClass().contains("cal-blue-text")) {
                    calendarHashMap.get(tableView).getStyleClass().add("cal-blue-text");
                }
            } else {
                calendarHashMap.get(tableView).getStyleClass().remove("cal-blue-text");
            }

            if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) {
                calendar.add(Calendar.DAY_OF_WEEK, 1);
            }
        }
        calendarTitleText += dateFormat.format(calendar.getTime());
        calendarTitleLabel.setText(calendarTitleText);
    }

    private void clearTableSelection() {
        sundayTableView.getSelectionModel().select(null);
        mondayTableView.getSelectionModel().select(null);
        tuesdayTableView.getSelectionModel().select(null);
        wednesdayTableView.getSelectionModel().select(null);
        thursdayTableView.getSelectionModel().select(null);
        fridayTableView.getSelectionModel().select(null);
        saturdayTableView.getSelectionModel().select(null);
    }

    private void createTableSelectionListener(TableColumn<Appointment, String> tableColumn, TableView<Appointment> tableView) {
        tableColumn.setCellValueFactory(new PropertyValueFactory<Appointment, String>("title"));
        tableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Appointment>() {
            @Override
            public void changed(ObservableValue<? extends Appointment> observable, Appointment oldValue, Appointment newValue) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    AppointmentsMonthlyController.setSelectedAppointment(newValue);
                    editButton.setDisable(false);
                    deleteButton.setDisable(false);
                }
            }
        });
    }

    @FXML private void navbarCustomersButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("Customers");
    }
    @FXML private void navbarReportsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("ReportAppointmentType");
    }
    @FXML private void navbarLogOutButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("LogIn");
    }

    // Toggle Buttons
    @FXML private void monthlyToggleButton(ActionEvent event) {
        try {
            DesktopScheduler.changeScenes("AppointmentsMonthly");
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }

    // Calendar Controls
    @FXML private void calendarPreviousButton(ActionEvent event) {
        calendar.add(Calendar.WEEK_OF_MONTH, -1);
        generateCalendar();
    }
    @FXML private void calendarNextButton(ActionEvent event) {
        calendar.add(Calendar.WEEK_OF_MONTH, 1);
        generateCalendar();
    }

    // Appointment Controls
    @FXML private void addButton(ActionEvent event) throws IOException{
        try {
            AddAppointmentController.setLastScene("AppointmentsWeekly");
            DesktopScheduler.changeScenes("AddAppointment");
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }
    @FXML private void editButton(ActionEvent event) {
        try {
            EditAppointmentController.setLastScene("AppointmentsWeekly");
            DesktopScheduler.changeScenes("EditAppointment");
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }
    @FXML private void deleteButton(ActionEvent event) {
        deleteAlert.showAndWait();
        if (deleteAlert.getResult() == ButtonType.YES) {

            System.out.println(Helper.changeColor("Deleting Appointment...", Helper.Color.YELLOW));

            String appointmentDelete = "DELETE FROM `appointment` WHERE `appointmentId` = ?";
            try (PreparedStatement deleteAppointment = Database.getConnection().prepareStatement(appointmentDelete)) {
                deleteAppointment.setInt(1, AppointmentsMonthlyController.getSelectedAppointment().getId());

                // Delete the record
                int rowsAffected = deleteAppointment.executeUpdate();
                System.out.println("\tRemoved Appointment: " + rowsAffected + " row affected.");
                if (rowsAffected == 0) System.out.println("\tAppointment Deletion Failed");

                // Update appointment array
                DesktopScheduler.getLocalData().deleteAppointment(AppointmentsMonthlyController.getSelectedAppointment());

                // Update the appointment tables
                clearTableSelection();
                generateCalendar();
                editButton.setDisable(true);
                deleteButton.setDisable(true);

                System.out.println(Helper.changeColor("Appointment deleted", Helper.Color.GREEN));
                System.out.println(DesktopScheduler.getLocalData().getAllAppointments().size() + " appointments in memory\n");


            } catch (SQLException e) {
                System.out.println(Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Set Edit and Delete buttons to disabled
        editButton.setDisable(true);
        deleteButton.setDisable(true);

        // Configure Calendar Title
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        String calendarTitleText = dateFormat.format(calendar.getTime());
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        calendarTitleText += " - " + dateFormat.format(calendar.getTime());
        calendarTitleLabel.setText(calendarTitleText);

        // Configure Calendar Map
        calendarHashMap.put(sundayTableView, sundayTableColumn);
        calendarHashMap.put(mondayTableView, mondayTableColumn);
        calendarHashMap.put(tuesdayTableView, tuesdayTableColumn);
        calendarHashMap.put(wednesdayTableView, wednesdayTableColumn);
        calendarHashMap.put(thursdayTableView, thursdayTableColumn);
        calendarHashMap.put(fridayTableView, fridayTableColumn);
        calendarHashMap.put(saturdayTableView, saturdayTableColumn);

        // Generate the calendar
        generateCalendar();

        // Configure tables
        createTableSelectionListener(sundayTableColumn, sundayTableView);
        createTableSelectionListener(mondayTableColumn, mondayTableView);
        createTableSelectionListener(tuesdayTableColumn, tuesdayTableView);
        createTableSelectionListener(wednesdayTableColumn, wednesdayTableView);
        createTableSelectionListener(thursdayTableColumn, thursdayTableView);
        createTableSelectionListener(fridayTableColumn, fridayTableView);
        createTableSelectionListener(saturdayTableColumn, saturdayTableView);
    }
}

//*******************************************************************
// LogInController
//
// This controls the interactions for LogIn.fxml. This is the first screen of the application and will register the
// user as "logged-in" if the entered password is correct and they are marked as active.
//*******************************************************************

package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import main.DesktopScheduler;
import models.Appointment;
import models.User;
import util.Helper;
import exceptions.LogInException;
import util.Logger;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.ResourceBundle;

public class LogInController implements Initializable {

    // Components
    @FXML private Button navbarLogInButton;
    @FXML private Label titleLabel;
    @FXML private Label usernameLabel;
    @FXML private Label passwordLabel;
    @FXML private Button submitButton;
    @FXML private TextField usernameTextField;
    @FXML private Label usernameErrorLabel;
    @FXML private PasswordField passwordPassField;
    @FXML private Label passwordErrorLabel;

    // Resource Bundle for Language Support
    private static final ResourceBundle rb = ResourceBundle.getBundle("lang/LogIn", Locale.forLanguageTag(Locale.getDefault().getCountry()));

    // Date Formats
    private final DateTimeFormatter singleMinuteFormatter = DateTimeFormatter.ofPattern("m");
    private final DateTimeFormatter isoDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
    private final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("h:mm a");

    // Current User
    private User currentUser = null;

    // Password Field Key Press
    @FXML private void passwordPassFieldKeyPress(KeyEvent event) {
        if (event.getCode().toString().equals("ENTER")) {
            submitButton.fire();
        }
    }

    // Upcoming Appointment Alert
    private void throwAppointmentAlert(Appointment appointment) {
        String appointmentAlertMessage = rb.getString("upcomingAppointment") + "\n";
        appointmentAlertMessage += "Name: " + appointment.getTitle() + "\n";
        appointmentAlertMessage += rb.getString("from") + ": " + appointment.getStart().format(timeFormatter) + "\n";
        appointmentAlertMessage += rb.getString("to") + ": " + appointment.getEnd().format(timeFormatter);
        Alert appointmentAlert = new Alert(Alert.AlertType.INFORMATION, appointmentAlertMessage, ButtonType.OK);
        appointmentAlert.showAndWait();
    }

    // Check for an appointment in the next 15 minutes
    private void checkForUpcomingAppointment() {

        int currentMinute = Integer.parseInt(LocalDateTime.now().format(singleMinuteFormatter));
        LocalDateTime currentTime = LocalDateTime.now();

        if ((currentMinute >= 15 && currentMinute <= 30) || (currentMinute >= 45)) {
            for (Appointment appointment:DesktopScheduler.getLocalData().getAppointmentsByDate(LocalDateTime.now().format(isoDateFormatter))) {
                if (appointment.getStart().isAfter(currentTime)) {

                    LocalDateTime tempDateTime = LocalDateTime.from(currentTime);
                    long hours = tempDateTime.until(appointment.getStart(), ChronoUnit.HOURS);
                    tempDateTime = tempDateTime.plusHours(hours);
                    long minutes = tempDateTime.until(appointment.getStart(), ChronoUnit.MINUTES);

                    if (hours == 0 && minutes <= 15) throwAppointmentAlert(appointment);
                }
            }
        }
    }

    // Alert if user is not active
    private static final Alert activeAlert = new Alert(Alert.AlertType.ERROR,
            rb.getString("activeError"), ButtonType.CLOSE);

    // Determine if the username matches a record, if the user is active, and if the password matches
    private boolean authorizeLogin(String username, String password) {

        User userLogin = DesktopScheduler.getLocalData().getUserByName(username);

        // Reset the fields
        usernameErrorLabel.setText("");
        usernameTextField.setStyle("-fx-border-color: inherit");
        passwordErrorLabel.setText("");
        passwordPassField.setStyle("-fx-border-color: inherit");

        // If username does not exist
        if (userLogin == null) {
            usernameErrorLabel.setText(rb.getString("usernameError"));
            usernameTextField.setStyle("-fx-border-color: red");
            Logger.log(Logger.Type.ERROR, "A Non-existent username \"" + usernameTextField.getText() + "\" was submitted.");
            throw new LogInException("Username does not exist.");

        // If user is not active
        } else if (!userLogin.isActive()) {
            activeAlert.showAndWait();
            Logger.log(Logger.Type.ERROR, "A log in of the inactive user \"" + usernameTextField.getText() + "\" was attempted.");
            return false;

        // If the password is incorrect
        } else if (!password.equals(userLogin.getPassword())) {
            passwordErrorLabel.setText(rb.getString("passwordError"));
            passwordPassField.setStyle("-fx-border-color: red");
            Logger.log(Logger.Type.ERROR, "An incorrect password was submitted.");
            throw new LogInException("Password is incorrect.");
        }

        currentUser = userLogin;
        return true;
    }

    // Submit Button
    @FXML private void submitButton(ActionEvent event) {

        if (authorizeLogin(usernameTextField.getText(), passwordPassField.getText())) {

            // Store the current user object in DesktopScheduler.java
            DesktopScheduler.setCurrentUser(currentUser);
            System.out.println(rb.getString("loggedInAs") + " "
                    + Helper.changeColor(DesktopScheduler.getCurrentUser().getUserName(), Helper.Color.BLUE) + "\n");
            Logger.log(Logger.Type.INFO, "A user has logged in as " + currentUser.getUserName() + ".");

            // Check for an upcoming appointment
            checkForUpcomingAppointment();

            try {
                DesktopScheduler.changeScenes("AppointmentsMonthly");
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Configure Labels
        navbarLogInButton.setText(rb.getString("login"));
        titleLabel.setText(rb.getString("login"));
        usernameLabel.setText(rb.getString("username"));
        usernameErrorLabel.setText("");
        passwordLabel.setText(rb.getString("password"));
        passwordErrorLabel.setText("");
        submitButton.setText(rb.getString("submit"));
    }
}

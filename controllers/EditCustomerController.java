//*******************************************************************
// EditCustomerController
//
// This controls the interactions for EditCustomer.fxml. This is used to modify an existing customer in the application.
//*******************************************************************

package controllers;

import exceptions.InvalidDataException;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.DesktopScheduler;
import models.Customer;
import util.Database;
import util.Helper;
import util.Validation;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class EditCustomerController implements Initializable {

    // Components
    @FXML private TextField nameTextField;
    @FXML private TextField address1TextField;
    @FXML private TextField address2TextField;
    @FXML private TextField cityTextField;
    @FXML private TextField zipTextField;
    @FXML private ChoiceBox<String> countryChoiceBox;
    @FXML private TextField phoneTextField;
    @FXML private CheckBox activeCheckBox;

    // State Variables
    private Customer selectedCustomer;
    private boolean isActive = true;

    private final Alert invalidDataAlert = new Alert(Alert.AlertType.ERROR,
            "One or more of the required form fields are empty.", ButtonType.CLOSE);

    private boolean checkFormFields() {

        // Reset the fields
        nameTextField.setStyle("-fx-border-color: inherit");
        address1TextField.setStyle("-fx-border-color: inherit");
        cityTextField.setStyle("-fx-border-color: inherit");
        zipTextField.setStyle("-fx-border-color: inherit");
        phoneTextField.setStyle("-fx-border-color: inherit");

        // Check if important fields are empty
        boolean isError = false;
        if (nameTextField.getText().isEmpty()) {
            nameTextField.setStyle("-fx-border-color: red");
            isError = true;
        }
        if (address1TextField.getText().isEmpty()) {
            address1TextField.setStyle("-fx-border-color: red");
            isError = true;
        }
        if (cityTextField.getText().isEmpty()) {
            cityTextField.setStyle("-fx-border-color: red");
            isError = true;
        }
        if (zipTextField.getText().isEmpty()) {
            zipTextField.setStyle("-fx-border-color: red");
            isError = true;
        }
        if (phoneTextField.getText().isEmpty()) {
            phoneTextField.setStyle("-fx-border-color: red");
            isError = true;
        }
        return !isError;
    }

    // Navigation Controls
    @FXML private void navbarAppointmentsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("AppointmentsMonthly");
    }
    @FXML private void navbarReportsButton(ActionEvent event) throws IOException {
        DesktopScheduler.changeScenes("ReportAppointmentType");
    }
    @FXML private void navbarLogOutButton(ActionEvent event) throws IOException {
        System.out.println("Logged out\n");
        DesktopScheduler.changeScenes("LogIn");
    }

    // Active Checkbox
    @FXML private void activeCheckBox(ActionEvent event) {
        isActive = activeCheckBox.isSelected();
    }

    // Save the Customer Information
    @FXML private void saveButton(ActionEvent event) {

        System.out.println(Helper.changeColor("Updating Customer...",Helper.Color.YELLOW));

        // Check if form is valid
        if (checkFormFields()) {

            // Static DateTime for following statements
            LocalDateTime currentDateTime = LocalDateTime.now();

            // Set the country for the selected customer
            selectedCustomer.setCountry(countryChoiceBox.getValue());

            // Check if city text field has changed
            if (!selectedCustomer.getCity().toLowerCase().equals(cityTextField.getText().toLowerCase())) {

                // Check if city already exists
                String cityQuery = "SELECT `cityId`, `city` FROM `city` WHERE `city` = LOWER(?)";
                try (PreparedStatement getCity = Database.getConnection().prepareStatement(cityQuery)) {
                    getCity.setString(1, cityTextField.getText().toLowerCase());

                    try (ResultSet results = getCity.executeQuery()) {

                        // If the city doesn't exist
                        if (!results.next()) {

                            // Add the city to the database
                            String cityInsert = "INSERT INTO `city`"
                                    + " (`city`, `countryId`, `createDate`, `createdBy`, `lastUpdateBy`)"
                                    + " VALUES (?, ?, ?, ?, ?)";
                            try (PreparedStatement createCity = Database.getConnection().prepareStatement(cityInsert,
                                    Statement.RETURN_GENERATED_KEYS)) {
                                createCity.setString(1, Helper.toTitleCase(cityTextField.getText()));
                                createCity.setInt(2, DesktopScheduler.getLocalData().getCountryId(countryChoiceBox.getValue()));
                                createCity.setObject(3, currentDateTime);
                                createCity.setString(4, DesktopScheduler.getCurrentUser().getUserName());
                                createCity.setString(5, DesktopScheduler.getCurrentUser().getUserName());

                                // Insert the record
                                int rowsAffected = createCity.executeUpdate();
                                System.out.println("\tCreated City: " + rowsAffected + " row affected.");
                                if (rowsAffected == 0) System.out.println("\tCity Creation Failed");

                                // Get the key of the insert
                                try (ResultSet generatedKey = createCity.getGeneratedKeys()) {
                                    if (generatedKey.next()) {
                                        selectedCustomer.setCityId(generatedKey.getInt(1));
                                    }
                                }

                                // Add to the selected customer object
                                selectedCustomer.setCity(Helper.toTitleCase(cityTextField.getText()));

                            } catch (SQLException e) {
                                System.out.println("\t" + Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
                            }

                        } else results.beforeFirst();

                        // If the city does exist
                        while (results.next()) {

                            // Set the city id to the existing city
                            selectedCustomer.setCityId(results.getInt(1));
                            System.out.println("\tUsing city: " + results.getString(2));

                            // Add to the selected customer object
                            selectedCustomer.setCity(results.getString(2));
                        }
                    }

                } catch (SQLException e) {
                    System.out.println("\t" + Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
                }

            } else System.out.println("\tCity has not changed");

            // If any of the address fields have changed
            if (!selectedCustomer.getAddress1().toLowerCase().equals(address1TextField.getText().toLowerCase())
                    || !selectedCustomer.getAddress2().toLowerCase().equals(address2TextField.getText().toLowerCase())
                    || !selectedCustomer.getPostalCode().toLowerCase().equals(zipTextField.getText().toLowerCase())
                    || !selectedCustomer.getPhone().toLowerCase().equals(zipTextField.getText().toLowerCase())) {

                // Update the address in the database
                String addressUpdate = "UPDATE `address`"
                        + " SET `address` = ?, `address2` = ?, `cityId` = ?, `postalCode` = ?,`phone` = ?, `lastUpdateBy` = ?"
                        + " WHERE `addressId` = ?";
                try (PreparedStatement updateAddress = Database.getConnection().prepareStatement(addressUpdate)) {
                    updateAddress.setString(1, Helper.toTitleCase(address1TextField.getText()));
                    updateAddress.setString(2, Helper.toTitleCase(address2TextField.getText()));
                    updateAddress.setInt(3, selectedCustomer.getCityId());
                    updateAddress.setString(4, zipTextField.getText());
                    updateAddress.setString(5, phoneTextField.getText());
                    updateAddress.setString(6, DesktopScheduler.getCurrentUser().getUserName());
                    updateAddress.setInt(7, selectedCustomer.getAddressId());

                    // Update the record
                    int rowsAffected = updateAddress.executeUpdate();
                    System.out.println("\tUpdated Address: " + rowsAffected + " row affected.");
                    if (rowsAffected == 0) System.out.println("\tAddress Update Failed");

                    // Update the existing customer object
                    selectedCustomer.setAddress1(Helper.toTitleCase(address1TextField.getText()));
                    selectedCustomer.setAddress2(Helper.toTitleCase(address2TextField.getText()));
                    selectedCustomer.setPostalCode(zipTextField.getText());
                    selectedCustomer.setPhone(phoneTextField.getText());

                } catch (SQLException e) {
                    System.out.println("\t" + Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
                }

            } else System.out.println("\tAddress has not changed");

            // If any of the customer fields have changed
            if (!selectedCustomer.getCustomerName().toLowerCase().equals(nameTextField.getText().toLowerCase())
                    || selectedCustomer.isActive() != isActive) {

                // Update the customer in the database
                String customerUpdate = "UPDATE `customer`"
                        + " SET `customerName` = ?, `active` = ?, `lastUpdateBy` = ? WHERE `customerId` = ? ";
                try (PreparedStatement updateCustomer = Database.getConnection().prepareStatement(customerUpdate,
                        Statement.RETURN_GENERATED_KEYS)) {
                    updateCustomer.setString(1, Helper.toTitleCase(nameTextField.getText()));
                    updateCustomer.setInt(2, (isActive ? 1 : 0));
                    updateCustomer.setString(3, DesktopScheduler.getCurrentUser().getUserName());
                    updateCustomer.setInt(4, selectedCustomer.getId());

                    // Update the record
                    int rowsAffected = updateCustomer.executeUpdate();
                    System.out.println("\tUpdated Customer: " + rowsAffected + " row affected.");
                    if (rowsAffected == 0) System.out.println("\tCustomer Update Failed");

                    // Add to the new customer object
                    selectedCustomer.setCustomerName(Helper.toTitleCase(nameTextField.getText()));
                    selectedCustomer.setActive(isActive);

                } catch (SQLException e) {
                    System.out.println("\t" + Helper.changeColor("SQLException: " + e.getMessage(), Helper.Color.RED));
                }

            } else System.out.println("\tCustomer name has not changed");

            System.out.println(Helper.changeColor("Customer updated", Helper.Color.GREEN) + "\n");

            // Go back to Customers screen
            try {
                DesktopScheduler.changeScenes("Customers");
            } catch (IOException e) {
                System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
            }

        } else {
            System.out.println(Helper.changeColor("Customer creation failed. Invalid data submitted.", Helper.Color.RED));
            invalidDataAlert.showAndWait();
            throw new InvalidDataException("Invalid Customer data was submitted.");
        }
    }

    // Cancel Button
    @FXML private void cancelButton(ActionEvent event) {
        try {
            DesktopScheduler.changeScenes("Customers");
        } catch (IOException e) {
            System.out.println(Helper.changeColor("IOException: " + e.getMessage(), Helper.Color.RED));
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Configure the ChoiceBox
        countryChoiceBox.setItems(DesktopScheduler.getLocalData().getCountries());
        countryChoiceBox.setValue(countryChoiceBox.getItems().get(0));

        // Get selected customer
        selectedCustomer = CustomersController.selectedCustomer;

        // Set max lengths to Text Fields
        Validation.formatTextField(nameTextField, 45, false);
        Validation.formatTextField(address1TextField, 50, false);
        Validation.formatTextField(address2TextField, 50, false);
        Validation.formatTextField(cityTextField, 50, false);
        Validation.formatTextField(zipTextField, 10, true);
        Validation.formatTextField(phoneTextField, 20, true);

        // Set the values of the Text Fields
        nameTextField.setText(selectedCustomer.getCustomerName());
        address1TextField.setText(selectedCustomer.getAddress1());
        address2TextField.setText(selectedCustomer.getAddress2());
        cityTextField.setText(selectedCustomer.getCity());
        zipTextField.setText(selectedCustomer.getPostalCode());
        countryChoiceBox.setValue(selectedCustomer.getCountry());
        phoneTextField.setText(selectedCustomer.getPhone());
        activeCheckBox.setSelected(isActive);
    }
}

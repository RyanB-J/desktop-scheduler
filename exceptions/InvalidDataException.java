//*******************************************************************
// InvalidDataException
//
// A custom Runtime exception. This fires when invalid or empty data fields are submitted.
//*******************************************************************

package exceptions;

public class InvalidDataException extends RuntimeException {

    public InvalidDataException(String message) {
        super(message);
    }
}

//*******************************************************************
// AppointmentConflictException
//
// A custom Runtime exception. This fires when there is a conflict between two appointments.
//*******************************************************************

package exceptions;

public class AppointmentConflictException extends RuntimeException {

    public AppointmentConflictException(String message) {
        super(message);
    }
}

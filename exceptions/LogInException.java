//*******************************************************************
// LogInException
//
// A custom Runtime exception. This fires when there is a problem logging in.
//*******************************************************************

package exceptions;

public class LogInException extends RuntimeException {

    public LogInException(String message) {
        super(message);
    }
}

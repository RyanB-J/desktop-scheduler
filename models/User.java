//*******************************************************************
// Customer
//
// Creates a user object when instantiated. This is used for LocalData.
//*******************************************************************

package models;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class User {

    private final SimpleIntegerProperty
            id = new SimpleIntegerProperty();

    private final SimpleStringProperty
            userName = new SimpleStringProperty(),
            password = new SimpleStringProperty();

    private final SimpleBooleanProperty
            active = new SimpleBooleanProperty();

    public User() {}

    public User(int id, String userName, String password, boolean active) {
        setId(id);
        setUserName(userName);
        setPassword(password);
        setActive(active);
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getUserName() {
        return userName.get();
    }

    public SimpleStringProperty userNameProperty() {
        return userName;
    }

    public void setUserName(String name) {
        this.userName.set(name);
    }

    public String getPassword() {
        return password.get();
    }

    public SimpleStringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public boolean isActive() {
        return active.get();
    }

    public SimpleBooleanProperty activeProperty() {
        return active;
    }

    public void setActive(boolean active) {
        this.active.set(active);
    }
}

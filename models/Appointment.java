//*******************************************************************
// Appointment
//
// Creates an appointment object when instantiated. This is used for LocalData.
//*******************************************************************

package models;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.time.LocalDateTime;

public class Appointment {

    private final SimpleIntegerProperty
            id = new SimpleIntegerProperty(),
            customerId = new SimpleIntegerProperty(),
            userId = new SimpleIntegerProperty();

    private final SimpleStringProperty
            title = new SimpleStringProperty(),
            description = new SimpleStringProperty(),
            location = new SimpleStringProperty(),
            contact = new SimpleStringProperty(),
            type = new SimpleStringProperty(),
            url = new SimpleStringProperty();

    private final SimpleObjectProperty<LocalDateTime>
            start = new SimpleObjectProperty<>(),
            end = new SimpleObjectProperty<>();

    public Appointment() {}

    public Appointment(int id, int customerId, int userId, String title, String description, String location,
                       String contact, String type, String url, LocalDateTime start, LocalDateTime end) {
        setId(id);
        setCustomerId(customerId);
        setUserId(userId);
        setTitle(title);
        setDescription(description);
        setLocation(location);
        setContact(contact);
        setType(type);
        setUrl(url);
        setStart(start);
        setEnd(end);
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getCustomerId() {
        return customerId.get();
    }

    public SimpleIntegerProperty customerIdProperty() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId.set(customerId);
    }

    public int getUserId() {
        return userId.get();
    }

    public SimpleIntegerProperty userIdProperty() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId.set(userId);
    }

    public String getTitle() {
        return title.get();
    }

    public SimpleStringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String getLocation() {
        return location.get();
    }

    public SimpleStringProperty locationProperty() {
        return location;
    }

    public void setLocation(String location) {
        this.location.set(location);
    }

    public String getContact() {
        return contact.get();
    }

    public SimpleStringProperty contactProperty() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact.set(contact);
    }

    public String getType() {
        return type.get();
    }

    public SimpleStringProperty typeProperty() {
        return type;
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public String getUrl() {
        return url.get();
    }

    public SimpleStringProperty urlProperty() {
        return url;
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public LocalDateTime getStart() {
        return start.get();
    }

    public SimpleObjectProperty<LocalDateTime> startProperty() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start.set(start);
    }

    public LocalDateTime getEnd() {
        return end.get();
    }

    public SimpleObjectProperty<LocalDateTime> endProperty() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end.set(end);
    }
}

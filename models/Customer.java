//*******************************************************************
// Customer
//
// Creates a customer object when instantiated. This is used for LocalData.
//*******************************************************************

package models;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Customer {

    private final SimpleIntegerProperty
            id = new SimpleIntegerProperty(),
            addressId = new SimpleIntegerProperty(),
            cityId = new SimpleIntegerProperty();

    private final SimpleStringProperty
            customerName = new SimpleStringProperty(),
            address1 = new SimpleStringProperty(),
            address2 = new SimpleStringProperty(),
            postalCode = new SimpleStringProperty(),
            phone = new SimpleStringProperty(),
            city = new SimpleStringProperty(),
            country = new SimpleStringProperty();

    private final SimpleBooleanProperty
            active = new SimpleBooleanProperty();

    public Customer() {}

    public Customer(int id, int addressId, int cityId, String customerName, String address1, String address2,
                    String postalCode, String phone, String city, String country, boolean active) {
        setId(id);
        setAddressId(addressId);
        setCityId(cityId);
        setCustomerName(customerName);
        setAddress1(address1);
        setAddress2(address2);
        setPostalCode(postalCode);
        setPhone(phone);
        setCity(city);
        setCountry(country);
        setActive(active);
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getAddressId() {
        return addressId.get();
    }

    public SimpleIntegerProperty addressIdProperty() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId.set(addressId);
    }

    public int getCityId() {
        return cityId.get();
    }

    public SimpleIntegerProperty cityIdProperty() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId.set(cityId);
    }

    public String getCustomerName() {
        return customerName.get();
    }

    public SimpleStringProperty customerNameProperty() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName.set(customerName);
    }

    public String getAddress1() {
        return address1.get();
    }

    public SimpleStringProperty address1Property() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1.set(address1);
    }

    public String getAddress2() {
        return address2.get();
    }

    public SimpleStringProperty address2Property() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2.set(address2);
    }

    public String getPostalCode() {
        return postalCode.get();
    }

    public SimpleStringProperty postalCodeProperty() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode.set(postalCode);
    }

    public String getPhone() {
        return phone.get();
    }

    public SimpleStringProperty phoneProperty() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public String getCity() {
        return city.get();
    }

    public SimpleStringProperty cityProperty() {
        return city;
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public String getCountry() {
        return country.get();
    }

    public SimpleStringProperty countryProperty() {
        return country;
    }

    public void setCountry(String country) {
        this.country.set(country);
    }

    public boolean isActive() {
        return active.get();
    }

    public SimpleBooleanProperty activeProperty() {
        return active;
    }

    public void setActive(boolean active) {
        this.active.set(active);
    }
}
